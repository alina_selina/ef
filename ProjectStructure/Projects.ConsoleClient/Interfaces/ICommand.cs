﻿using System.Threading.Tasks;

namespace Projects.ConsoleClient.Interfaces
{
    public interface ICommand
    {
        Task Execute();
    }
}
