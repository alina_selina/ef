﻿using static Projects.ConsoleClient.Utils.ConsoleUtils;
using PostSharp.Aspects;
using PostSharp.Serialization;
using System;
using System.Reflection;

namespace Projects.ConsoleClient.Decorators
{
    [PSerializable]
    public class TaskCanceledHandler : OnExceptionAspect
    {
        private Type _type;

        public TaskCanceledHandler(Type type)
        {
            _type = type;
        }

        public override Type GetExceptionType(MethodBase method) => _type;

        public override void OnException(MethodExecutionArgs args)
        {
            WriteErrorMessage("\nTimeout", true);
            WaitForAnyKeyPress();
            args.FlowBehavior = FlowBehavior.Return;
        }
    }
}
