﻿using PostSharp.Aspects;
using PostSharp.Serialization;
using System;
using System.Reflection;
using static Projects.ConsoleClient.Utils.ConsoleUtils;

namespace Projects.ConsoleClient.Decorators
{
    [PSerializable]
    public class HttpErrorHandler : OnExceptionAspect
    {
        private Type _type;

        public HttpErrorHandler(Type type)
        {
            _type = type;
        }

        public override Type GetExceptionType(MethodBase method) => _type;

        public override void OnException(MethodExecutionArgs args)
        {
            WriteErrorMessage($"\nError: {args.Exception.Message}", true);
            WaitForAnyKeyPress();
            args.FlowBehavior = FlowBehavior.Return;
        }
    }
}
