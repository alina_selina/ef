﻿using PostSharp.Aspects;
using PostSharp.Serialization;
using Projects.ConsoleClient.Utils;

namespace Projects.ConsoleClient.Decorators
{
    [PSerializable]
    public class Logger : OnExceptionAspect
    {
        private string _path;

        public Logger(string path)
        {
            _path = path;
        }

        public override void OnException(MethodExecutionArgs args)
        {
            FileManager.WriteFileAsync(args.Exception.Message, _path);
            args.FlowBehavior = FlowBehavior.Return;
        }
    }
}
