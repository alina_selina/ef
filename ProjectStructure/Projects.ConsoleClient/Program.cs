﻿using Projects.ConsoleClient.Commands;
using Projects.ConsoleClient.DTO;
using Projects.ConsoleClient.Services;
using System;
using System.Collections.Generic;
using System.Media;
using System.Threading.Tasks;

namespace Projects.ConsoleClient
{
    class Program
    {
        private static Dictionary<int, Operation> _mainMenu;
        private static string _header = "QUERY SERVICE";

        private static HttpService httpService;

        static Program()
        {
            httpService = new HttpService()
            {
                BaseAddress = new Uri(Settings.BaseAddress),
                Timeout = TimeSpan.FromMilliseconds(Settings.Timeout)
            };

            InitializeMenu();
        }

        static async Task Main()
        {
            await StartMenu(_mainMenu, _header);
        }

        static async Task StartMenu(Dictionary<int, Operation> menu, string header)
        {
            while (true)
            {
                Console.Clear();
                Menu.ShowHeader(header);
                Menu.Show(menu);
                await Menu.SelectOperation(menu);
            }
        }

        static void InitializeMenu()
        {
            _mainMenu = new Dictionary<int, Operation>
            {
               {1, new Operation("ShowProjectTaskCountByUserId", new ShowProjectTaskCountByUserId(httpService, "queries/ProjectTaskCount"))},
               {2, new Operation("ShowUserTasksByUserId", new ShowUserTasksByUserId(httpService, "queries/UserTasks"))},
               {3, new Operation("ShowUserTasksCurrentYearByUserId", new ShowUserTasksCurrentYearByUserId(httpService, "queries/UserTasksCurrentYear"))},
               {4, new Operation("ShowTeams", new ShowTeams(httpService, "queries/TeamsByCondition"))},
               {5, new Operation("ShowUsers", new ShowUsers(httpService, "queries/UsersWithTasks"))},
               {6, new Operation("ShowUserStruct", new ShowUserStruct(httpService, "queries/UserStruct"))},
               {7, new Operation("ShowProjectStruct", new ShowProjectStruct(httpService, "queries/ProjectStruct"))},
               {8, new Operation("WriteMarkRandomTaskWithDelay", new WriteMarkRandomTaskWithDelay(httpService, "Tasks"), false) },
               {9, new Operation("Quit", new Command(()=>
               {
                   new SoundPlayer($"{Settings.ResoursesPath}that'sAllFolks.wav").PlaySync();
                   Environment.Exit(0);
               })) },
            };
        }
    }
}