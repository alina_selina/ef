﻿using Projects.ConsoleClient.Interfaces;
using static System.Console;
using static Projects.ConsoleClient.Utils.ConsoleUtils;
using Projects.ConsoleClient.DTO;
using Newtonsoft.Json;
using System.Collections.Generic;
using Projects.ConsoleClient.Decorators;
using System.Threading.Tasks;
using System.Net.Http;

namespace Projects.ConsoleClient.Commands
{
    class ShowTeams : HttpCommand
    {
        public ShowTeams(IHttpService httpService, string path) : base(httpService, path)
        {
        }

        public ShowTeams(IHttpService httpService, string path, int timeout) : base(httpService, path, timeout)
        {
        }

        [TaskCanceledHandler(typeof(TaskCanceledException))]
        [HttpErrorHandler(typeof(HttpRequestException))]
        public override async Task Execute()
        {
            var response = await Client.Get(Path, Timeout);
            if (response != null && !response.IsSuccessStatusCode)
            {
                await ShowHttpResponse<HttpErrorResponse>(response);
                return;
            }

            var result = JsonConvert.DeserializeObject<List<TeamsDTO>>(await response.Content.ReadAsStringAsync());

            WriteLine("\nResult:\n");
            if (result.Count > 0)
                result.ForEach(v => WriteLine(v.ToString()));
            else
                WriteLine("Not yet");

            WaitForAnyKeyPress();
        }
    }
}

