﻿using Projects.ConsoleClient.Interfaces;
using static System.Console;
using static Projects.ConsoleClient.Utils.ConsoleUtils;
using static Projects.ConsoleClient.Validators.IdValidator;
using Projects.ConsoleClient.DTO;
using Newtonsoft.Json;
using Projects.ConsoleClient.Decorators;
using System.Threading.Tasks;
using System.Net.Http;

namespace Projects.ConsoleClient.Commands
{
    class ShowUserStruct : HttpCommand
    {
        public ShowUserStruct(IHttpService httpService, string path) : base(httpService, path)
        {
        }

        public ShowUserStruct(IHttpService httpService, string path, int timeout) : base(httpService, path, timeout)
        {
        }

        [TaskCanceledHandler(typeof(TaskCanceledException))]
        [HttpErrorHandler(typeof(HttpRequestException))]
        public override async Task Execute()
        {
            if (!TryGetIdInput(out int id))
                return;

            var response = await Client.Get($"{Path}/{id}", Timeout);
            if (response != null && !response.IsSuccessStatusCode)
            {
                await ShowHttpResponse<HttpErrorResponse>(response);
                return;
            }

            var result = JsonConvert.DeserializeObject<UserStructDTO>(await response.Content.ReadAsStringAsync());

            WriteLine(result.ToString());

            WaitForAnyKeyPress();
        }
    }
}
