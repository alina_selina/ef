﻿using Projects.ConsoleClient.Interfaces;
using Projects.ConsoleClient.DTO;
using Newtonsoft.Json;
using Projects.ConsoleClient.Decorators;
using System.Threading.Tasks;
using System.Net.Http;
using System;
using System.Linq;
using System.Timers;
using System.Collections.Generic;
using CollectionsAndLINQ.Enums;
using System.Text;
using Projects.ConsoleClient.Utils;

namespace Projects.ConsoleClient.Commands
{
    public class WriteMarkRandomTaskWithDelay : HttpCommand
    {
        public WriteMarkRandomTaskWithDelay(IHttpService httpService, string path) : base(httpService, path)
        { }

        public WriteMarkRandomTaskWithDelay(IHttpService httpService, string path, int timeout) : base(httpService, path, timeout)
        { }

        private Random _random = new();

        [Logger("errors.txt")]
        private async Task<(bool Available, int? Id)> TryUpdateRandomTask()
        {
            using var getResponse = await Client.Get(Path, Timeout);

            if (getResponse != null && !getResponse.IsSuccessStatusCode)
                return (false, null);

            var result = JsonConvert.DeserializeObject<List<TaskDTO>>(await getResponse.Content.ReadAsStringAsync());

            var task = result.OrderBy(x => _random.Next()).FirstOrDefault();
            task.State = TaskState.Done;

            using var data = new StringContent(JsonConvert.SerializeObject(task), Encoding.UTF8, "application/json");
            using var putResponse = await Client.Put(Path, data, Timeout);

            if (putResponse != null && !putResponse.IsSuccessStatusCode)
                return (false, null);

            return (true, task.Id);
        }

        private async Task<int> MarkRandomTaskWithDelay(int delay = 1000)
        {
            var tcs = new TaskCompletionSource<int>();
            var timer = new Timer(delay)
            {
                AutoReset = false
            };

            timer.Elapsed += async (sender, args) =>
            {
                timer.Dispose();

                var result = await TryUpdateRandomTask();
                if (!result.Available)
                    tcs.SetException(new Exception("Operation failed."));
                else
                    tcs.SetResult((int)result.Id);
            };

            timer.Start();

            return await tcs.Task;
        }

        [Logger("errors.txt")]
        public override async Task Execute()
        {
            while (true)
            {
                var id = await MarkRandomTaskWithDelay();
                await FileManager.WriteFileAsync(id.ToString(), "updatedTasks.txt");
                await Task.Delay(5000);
            }
        }
    }
}
