﻿using Projects.ConsoleClient.Interfaces;
using System;
using System.Threading.Tasks;

namespace Projects.ConsoleClient.Commands
{
    class Command : ICommand
    {
        private Action Action { get; }

        public Command(Action action)
        {
            Action = action;
        }

        public async Task Execute()
        {
            await Task.Run(() => Action.Invoke());
        }
    }
}
