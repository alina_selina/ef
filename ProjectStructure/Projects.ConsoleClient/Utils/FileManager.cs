﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Projects.ConsoleClient.Utils
{
    public static class FileManager
    {
        public static async Task WriteFileAsync(string message, string fileName)
        {
            await File.AppendAllTextAsync(Path.Combine(Settings.ResoursesPath, fileName), 
                                          $"[{DateTime.Now}]: {message}{Environment.NewLine}");
        }
    }
}
