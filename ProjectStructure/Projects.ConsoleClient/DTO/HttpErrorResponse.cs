﻿using Newtonsoft.Json;
using System.Net;

namespace Projects.ConsoleClient.DTO
{
    public class HttpErrorResponse
    {
        [JsonProperty("statusCode")]
        public HttpStatusCode StatusCode { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        public override string ToString()
        {
            return $"Error ({(int)StatusCode}): {Message}.";
        }
    }
}
