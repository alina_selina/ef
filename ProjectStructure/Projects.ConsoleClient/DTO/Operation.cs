﻿using Projects.ConsoleClient.Interfaces;

namespace Projects.ConsoleClient.DTO
{
    public struct Operation
    {
        public string Name { get; set; }
        public ICommand Command { get; set; }
        public bool Awaitable { get; set; }

        public Operation(string name, ICommand method, bool awaitable = true)
        {
            Name = name;
            Command = method;
            Awaitable = awaitable;
        }
    }
}
