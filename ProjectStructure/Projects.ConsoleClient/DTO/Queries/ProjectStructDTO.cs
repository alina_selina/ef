﻿namespace Projects.ConsoleClient.DTO
{
    public class ProjectStructDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestTask { get; set; }
        public TaskDTO ShortestTask { get; set; }
        public int UserCount { get; set; }

        public override string ToString()
        {
            return $"{Project} | longestTask: {LongestTask} |  shortestTask: {ShortestTask} | userCount: {UserCount}\n";
        }
    }
}
