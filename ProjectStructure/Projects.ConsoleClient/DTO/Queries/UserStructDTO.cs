﻿namespace Projects.ConsoleClient.DTO
{
    public class UserStructDTO
    {
        public UserDTO User { get; set; }
        public ProjectDTO LastProject { get; set; }
        public int LastProjectTaskCount { get; set; }
        public int UnfinishedTaskCount { get; set; }
        public TaskDTO LongestTask { get; set; }

        public override string ToString()
        {
            return $"User: {User}\nlastProject: {LastProject}\nlastProjectTaskCount: {LastProjectTaskCount}\n" +
                $"unfinishedTaskCount: {UnfinishedTaskCount}\nlongestTask: {LongestTask}";
        }
    }
}
