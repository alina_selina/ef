﻿using System.Collections.Generic;
using System.Linq;

namespace Projects.ConsoleClient.DTO
{
    public class TeamsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UserDTO> Users { get; set; }

        public override string ToString()
        {
            return $"\nId - {Id}, name - {Name}, \nusers:\n{string.Join("\n", Users.Select(u=>u.ToString()))}";
        }
    }
}
