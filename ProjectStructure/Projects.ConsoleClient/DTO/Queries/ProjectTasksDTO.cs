﻿namespace Projects.ConsoleClient.DTO
{
    public class ProjectTasksDTO
    {
        public ProjectDTO Project { get; set; }
        public int TaskCount { get; set; }

        public override string ToString()
        {
            return $"{Project} : TaskCount {TaskCount}";
        }
    }
}
