﻿using System;

namespace Projects.ConsoleClient.DTO
{
    public class TeamDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            return $"TeamId: {Id}, Name: {Name}, CreatedAt: {CreatedAt}";
        }
    }
}
