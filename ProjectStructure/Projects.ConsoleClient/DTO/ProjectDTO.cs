﻿using System;

namespace Projects.ConsoleClient.DTO
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            return $"ProjectId: {Id}, AuthorId: {AuthorId}, TeamId: {TeamId}, Name: {Name}, Deadline: {Deadline}, CreatedAt: {CreatedAt}";
        }
    }
}
