﻿using System;

namespace Projects.ConsoleClient.DTO
{
    public class UserDTO
    {
        public int Id { get; set; }
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }

        public override string ToString()
        {
            return $"UserId: {Id}, TeamId: {TeamId}, FirstName: {FirstName}, LastName: {LastName}, Email: {Email}, BirthDay: {BirthDay}";
        }
    }
}
