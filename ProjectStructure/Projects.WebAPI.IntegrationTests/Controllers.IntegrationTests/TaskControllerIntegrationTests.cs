﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Projects.WebAPI.IntegrationTests.Controllers.Tests
{
    [Collection("Non-Parallel Collection")]
    public class TaskControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;

        public TaskControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
        }

        [Fact]
        public async Task Delete_WhenTaskExist_ThenResponseWithCode204()
        {
            var httpResponse = await _client.DeleteAsync($"api/tasks/1");

            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);

            var httpResponseGetById = await _client.GetAsync($"api/tasks/1");

            Assert.Equal(HttpStatusCode.NotFound, httpResponseGetById.StatusCode);
        }

        [Fact]
        public async Task Delete_WhenTaskUnexist_ThenResponseWithCode404()
        {
            var httpResponse = await _client.DeleteAsync($"api/tasks/29");

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }
    }
}
