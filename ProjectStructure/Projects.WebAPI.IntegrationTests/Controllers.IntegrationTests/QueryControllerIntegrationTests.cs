﻿using Projects.Common.DTO.Query;
using Projects.Common.DTO.Task;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Projects.WebAPI.IntegrationTests.Controllers.IntegrationTests
{
    [Collection("Non-Parallel Collection")]
    public class QueryControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;

        public QueryControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
        }

        [Fact]
        public async Task GetProjectStruct_WhenProjectIdIs5_ThenResponseWithCode200AndCorrespondedBody()
        {
            var httpResponse = await _client.GetAsync("api/queries/ProjectStruct/5");

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var projectStruct = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectStructDTO>(stringResponse);

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Equal(5, projectStruct.Project.Id);
            Assert.Equal(19, projectStruct.LongestTask.Id);
            Assert.Equal(7, projectStruct.ShortestTask.Id);
            Assert.Equal(1, projectStruct.UserCount);
        }

        [Fact]
        public async Task GetProjectStruct_WhenUnexistingProject_ThenResponseWithCode400()
        {
            var httpResponse = await _client.GetAsync("api/queries/ProjectStruct/50");
            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }

        [Fact]
        public async Task GetUnfinishedTasks_WhenUserIdIs20_ThenResponseWithCode200AndCorrespondedBody()
        {
            var httpResponse = await _client.GetAsync("api/queries/UnfinishedTasks/20");

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var unfinishedTasks = Newtonsoft.Json.JsonConvert.DeserializeObject<List<TaskDTO>>(stringResponse);

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Single(unfinishedTasks);
            Assert.Equal(6, unfinishedTasks.First().Id);
        }

        [Fact]
        public async Task GetUnfinishedTasks_WhenUnexistingUser_ThenResponseWithCode400()
        {
            var httpResponse = await _client.GetAsync("api/queries/UnfinishedTasks/21");
            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }
    }
}
