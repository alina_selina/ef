using Projects.Common.DTO;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Projects.WebAPI.IntegrationTests.Controllers.Tests
{
    [Collection("Non-Parallel Collection")]
    public class ProjectControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private CustomWebApplicationFactory<Startup> _factory;

        public ProjectControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
            _factory = factory;
        }

        public void Dispose()
        {
        }

        [Fact]
        public async Task Create_WhenNewProjectWithExistingAuthorAndTeam_ThenResponseWithCode200AndCorrespondedBody()
        {
            var project = new ProjectCreateDTO()
            {
                AuthorId = 1,
                Name = "newProject",
                Description = "text",
                Deadline = new DateTime(2021, 10, 1),
                TeamId = 1
            };

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(project);
            var httpResponse = await _client.PostAsync("api/projects", new StringContent(json, Encoding.UTF8, "application/json"));

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdProject = Newtonsoft.Json.JsonConvert.DeserializeObject<ProjectDTO>(stringResponse);

            await _client.DeleteAsync($"api/projects/{createdProject.Id}");

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Equal(project.AuthorId, createdProject.AuthorId);
            Assert.Equal(project.TeamId, createdProject.TeamId);
            Assert.Equal(project.Name, createdProject.Name);
        }

        [Theory]
        [InlineData("{\"AuthorId\":1,\"TeamId\":21,\"Name\":\"newProject\",\"Description\":\"text\",\"Deadline\":\"2021-10-01T00:00:00\"}")]//unexisting team
        [InlineData("{\"AuthorId\":21,\"TeamId\":1,\"Name\":\"newProject\",\"Description\":\"text\",\"Deadline\":\"2021-10-01T00:00:00\"}")]//unexisting user
        [InlineData("{\"AuthorId\":1,\"TeamId\":1,\"Name\":\"n\",\"Description\":\"text\",\"Deadline\":\"2021-10-01T00:00:00\"}")]//short name
        [InlineData("data")]
        [InlineData("")]
        public async Task Create_WhenWrongRequestBody_ThenResponseWithCode400(string json)
        {
            var httpResponse = await _client.PostAsync("api/projects", new StringContent(json, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }
    }
}
