﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace Projects.WebAPI.IntegrationTests.Controllers.Tests
{
    [Collection("Non-Parallel Collection")]
    public class UserControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;

        public UserControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
        }

        [Fact]
        public async Task Delete_WhenUserExist_ThenResponseWithCode204()
        {
            var httpResponse = await _client.DeleteAsync($"api/users/9");

            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);

            var httpResponseGetById = await _client.GetAsync($"api/users/9");

            Assert.Equal(HttpStatusCode.NotFound, httpResponseGetById.StatusCode);
        }

        [Fact]
        public async Task Delete_WhenUserUnexist_ThenResponseWithCode404()
        {
            var httpResponse = await _client.DeleteAsync($"api/users/29");

            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }
    }
}
