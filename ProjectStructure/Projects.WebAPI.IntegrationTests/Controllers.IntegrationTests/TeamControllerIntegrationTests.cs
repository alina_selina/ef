﻿using Projects.Common.DTO.Team;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Projects.WebAPI.IntegrationTests.Controllers.Tests
{
    [Collection("Non-Parallel Collection")]
    public class TeamControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;

        public TeamControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
        }

        [Fact]
        public async Task Create_WhenNewTeam_ThenResponseWithCode200()
        {
            var team = new TeamCreateDTO()
            {
                Name = "newTeam"
            };

            var json = Newtonsoft.Json.JsonConvert.SerializeObject(team);
            var httpResponse = await _client.PostAsync("api/teams", new StringContent(json, Encoding.UTF8, "application/json"));

            var stringResponse = await httpResponse.Content.ReadAsStringAsync();
            var createdTeam = Newtonsoft.Json.JsonConvert.DeserializeObject<TeamDTO>(stringResponse);

            await _client.DeleteAsync($"api/teams/{createdTeam.Id}");

            Assert.Equal(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.Equal(team.Name, createdTeam.Name);
        }

        [Theory]
        [InlineData("{\"Name\":\"n\"}")]//short name
        [InlineData("data")]
        [InlineData("")]
        public async Task Create_WhenWrongRequestBody_ThenResponseWithCode400(string json)
        {
            var httpResponse = await _client.PostAsync("api/teams", new StringContent(json, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }
    }
}
