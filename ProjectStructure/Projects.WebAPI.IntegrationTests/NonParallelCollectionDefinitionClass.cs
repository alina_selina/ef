﻿using Xunit;

namespace Projects.WebAPI.IntegrationTests
{
    [CollectionDefinition("Non-Parallel Collection", DisableParallelization = true)]
    public class NonParallelCollectionDefinitionClass
    {
    }
}
