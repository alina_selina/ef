﻿namespace Projects.Common.DTO.Query
{
    public class UserTasksCurrentYearDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
