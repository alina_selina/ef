﻿using Projects.Common.DTO.User;
using System;
using System.Collections.Generic;

namespace Projects.Common.DTO.Query
{
    public class TeamsDTO: IEquatable<TeamsDTO>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UserDTO> Users { get; set; }

        public bool Equals(TeamsDTO other)
        {
            return Id.Equals(other.Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
