﻿using System;

namespace Projects.Common.DTO.Query
{
    public class ProjectTasksDTO : IEquatable<ProjectTasksDTO>
    {
        public ProjectDTO Project { get; set; }
        public int TaskCount { get; set; }

        public bool Equals(ProjectTasksDTO other)
        {
            return Project.Id.Equals(other.Project.Id);
        }

        public override int GetHashCode()
        {
            return Project.Id.GetHashCode();
        }
    }
}
