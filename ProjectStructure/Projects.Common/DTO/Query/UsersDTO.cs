﻿using Projects.Common.DTO.Task;
using Projects.Common.DTO.User;
using System.Collections.Generic;

namespace Projects.Common.DTO.Query
{
    public class UsersDTO
    {
        public UserDTO User { get; set; }
        public List<TaskDTO> Tasks { get; set; }
    }
}
