﻿using Projects.Common.Enums;
using Projects.Common.Interfaces;
using System;

namespace Projects.Common.DTO.Task
{    
    public class TaskDTO : ITask
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
