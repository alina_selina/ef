﻿using Projects.Common.Interfaces;
using System;

namespace Projects.Common.DTO
{

    public class UserCreateDTO : IUser
    {
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime BirthDay { get; set; }
    }
}
