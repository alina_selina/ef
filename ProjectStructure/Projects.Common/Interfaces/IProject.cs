﻿namespace Projects.Common.Interfaces
{
    public interface IProject
    {
        public int AuthorId { get; set; }
        public int TeamId { get; set; }
    }
}
