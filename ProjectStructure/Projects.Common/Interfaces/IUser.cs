﻿namespace Projects.Common.Interfaces
{
    public interface IUser
    {
        public int? TeamId { get; set; }
    }
}
