﻿using Projects.Common.Enums;

namespace Projects.Common.Interfaces
{
    public interface ITask
    {
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public TaskState State { get; set; }
    }
}
