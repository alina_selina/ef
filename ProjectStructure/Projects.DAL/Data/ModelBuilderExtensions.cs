﻿using Bogus;
using CollectionsAndLINQ.Entities;
using Microsoft.EntityFrameworkCore;
using Projects.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Projects.DAL.Data
{
    public static class ModelBuilderExtensions
    {
        private const int EntityCount = 20;

        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Task>()
                .HasOne(t => t.Performer)
                .WithMany()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<User>()
                .HasOne(p => p.Team)
                .WithMany()
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Team>()
                .HasMany(t => t.Users)
                .WithOne(u => u.Team)
                .HasForeignKey(u => u.TeamId);

            modelBuilder.Entity<Project>()
                .Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(100);

            modelBuilder.Entity<Task>()
                .Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            modelBuilder.Entity<Team>()
                .Property(t => t.Name)
                .IsRequired();

            modelBuilder.Entity<User>()
                .Property(u => u.FirstName)
                .IsRequired();

            modelBuilder.Entity<User>()
                .Property(u => u.LastName)
                .IsRequired();

            modelBuilder.Entity<User>()
                .HasAlternateKey(u => u.Email);
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            var teams = GenerateRandomTeams();
            var users = GenerateRandomUsers(teams);
            var projects = GenerateRandomProjects(users, teams);
            var tasks = GenerateRandomTasks(projects, users);

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
        }

        public static List<Project> GenerateRandomProjects(List<User> users, List<Team> teams)
        {
            int pId = 1;

            var projectsFake = new Faker<Project>()
                .RuleFor(u => u.Id, f => pId++)
                .RuleFor(u => u.AuthorId, f => f.PickRandom(users).Id)
                .RuleFor(u => u.Deadline, f => f.Date.Between(new DateTime(2020, 8, 1), f.Date.Future()))
                .RuleFor(u => u.Description, f => f.Lorem.Sentence())
                .RuleFor(u => u.Name, f => f.Lorem.Word())
                .RuleFor(u => u.TeamId, f => f.PickRandom(teams).Id)
                .RuleFor(u => u.CreatedAt, f => f.Date.Between(new DateTime(2017, 8, 1), new DateTime(2020, 8, 1)));

            var projects = projectsFake.Generate(EntityCount);

            return projects.ToList();
        }

        public static List<Task> GenerateRandomTasks(List<Project> projects, List<User> users)
        {
            int taskId = 1;
            var tasks = new List<Task>();
            Project project;
            List<User> projectUsers;
            var f = new Faker();

            for (int _ = 0; _ < EntityCount; _++)
            {
                while (true)
                {
                    project = f.PickRandom(projects);
                    projectUsers = users.Where(u => u.TeamId == project.TeamId).ToList();

                    if (projectUsers.Count > 0)
                        break;
                }

                var cDate = f.Date.Between(project.CreatedAt, project.Deadline);
                var fDate = f.Date.Between(cDate, project.Deadline);

                var tasksFake = new Faker<Task>()
                   .RuleFor(pi => pi.Id, f => taskId++)
                   .RuleFor(pi => pi.Name, f => f.Lorem.Sentence())
                   .RuleFor(pi => pi.PerformerId, f => f.PickRandom(projectUsers).Id)
                   .RuleFor(pi => pi.Description, f => f.Lorem.Sentence())
                   .RuleFor(pi => pi.CreatedAt, cDate)
                   .RuleFor(pi => pi.State, f => f.PickRandom<TaskState>())
                   .RuleFor(pi => pi.ProjectId, project.Id)
                   .RuleFor(pi => pi.FinishedAt, fDate);

                tasks.Add(tasksFake.Generate());
            }

            return tasks;
        }

        public static List<User> GenerateRandomUsers(List<Team> teams)
        {
            int userId = 1;

            var testUsersFake = new Faker<User>()
                .RuleFor(u => u.Id, f => userId++)
                .RuleFor(u => u.FirstName, f => f.Person.FirstName)
                .RuleFor(u => u.LastName, f => f.Person.LastName)
                .RuleFor(u => u.Email, f => f.Internet.Email())
                .RuleFor(u => u.RegisteredAt, f => f.Date.Past())
                .RuleFor(u => u.BirthDay, f => f.Date.Between(new DateTime(1960, 10, 1), new DateTime(2015, 10, 1)))
                .RuleFor(u => u.TeamId, f => f.PickRandom(teams).Id);

            var generatedUsers = testUsersFake.Generate(EntityCount);

            return generatedUsers.ToList();
        }

        public static List<Team> GenerateRandomTeams()
        {
            int teamId = 1;

            var teamsFake = new Faker<Team>()
               .RuleFor(pi => pi.Id, f => teamId++)
               .RuleFor(pi => pi.Name, f => f.Lorem.Word());

            var teams = teamsFake.Generate(EntityCount);

            return teams.ToList();
        }
    }
}