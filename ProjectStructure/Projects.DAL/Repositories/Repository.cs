﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Projects.Common.Interfaces;
using Projects.DAL.Data;
using Projects.DAL.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Projects.DAL.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
    {
        protected readonly ProjectsContext Context;
        protected readonly DbSet<TEntity> Set;

        public Repository(ProjectsContext context)
        {
            Context = context;
            Set = Context.Set<TEntity>();
        }

        public virtual async Task<TEntity> CreateAsync(TEntity entity)
        {
            await Set.AddAsync(entity);
            return entity;
        }

        public async Task DeleteAsync(object id)
        {
            var entityToDelete = await Set.FindAsync(id);

            if (entityToDelete != null)
                Delete(entityToDelete);
        }

        public void Delete(TEntity entity)
        {
            Set.Remove(entity);
        }

        public async Task<ICollection<TEntity>> GetAllAsync(Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null,
                                                            bool disableTracking = false)
        {
            IQueryable<TEntity> result = Set;

            if (disableTracking)
                result = result.AsNoTracking();

            if (include != null)
                result = include(result);

            return await result.ToListAsync();
        }

        public virtual async Task<TEntity> GetFirstOrDefaultAsync(Expression<Func<TEntity, bool>> filter = null,
                                                                  Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null,
                                                                  bool disableTracking = false)
        {
            IQueryable<TEntity> result = Set;

            if (disableTracking)
                result = result.AsNoTracking();

            if (filter != null)
                result = result.Where(filter);

            if (include != null)
                result = include(result);

            return await result.FirstOrDefaultAsync();
        }

        public virtual async Task<TEntity> UpdateAsync(TEntity entity)
        {
            var entityToUpdate = await Set.FindAsync(entity.Id);

            if (entityToUpdate == null)
                return null;

            Context.Entry(entityToUpdate).State = EntityState.Detached;
            return Set.Update(entity).Entity;
        }
    }
}