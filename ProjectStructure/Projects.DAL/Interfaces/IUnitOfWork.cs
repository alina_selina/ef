﻿using Projects.DAL.Interfaces.Repositories;
using System;
using System.Threading.Tasks;

namespace Projects.DAL.Interfaces
{
    public interface IUnitOfWork : IRepositoryFactory, IDisposable
    {
        Task<int> SaveChangesAsync();
    }
}
