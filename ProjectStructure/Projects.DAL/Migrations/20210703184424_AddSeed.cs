﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Projects.DAL.Migrations
{
    public partial class AddSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 7, 3, 21, 44, 22, 643, DateTimeKind.Local).AddTicks(3555), "et" },
                    { 18, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "ea" },
                    { 17, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "ratione" },
                    { 16, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "qui" },
                    { 15, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "omnis" },
                    { 14, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "eius" },
                    { 13, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "expedita" },
                    { 12, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "et" },
                    { 11, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "deleniti" },
                    { 10, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "rerum" },
                    { 9, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "perferendis" },
                    { 8, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "ullam" },
                    { 7, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "ipsum" },
                    { 6, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "dolores" },
                    { 5, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "delectus" },
                    { 4, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "consequatur" },
                    { 3, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "nesciunt" },
                    { 2, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "voluptas" },
                    { 19, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "non" },
                    { 20, new DateTime(2021, 7, 3, 21, 44, 22, 690, DateTimeKind.Local).AddTicks(1556), "molestias" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "CreatedAt", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 17, new DateTime(2000, 11, 6, 16, 22, 23, 153, DateTimeKind.Unspecified).AddTicks(3402), new DateTime(2021, 1, 12, 12, 16, 8, 521, DateTimeKind.Local).AddTicks(192), "Bertrand.Keebler98@hotmail.com", "Bryant", "Kirlin", new DateTime(2021, 1, 12, 12, 16, 8, 521, DateTimeKind.Local).AddTicks(192), 1 },
                    { 18, new DateTime(1967, 3, 5, 4, 26, 16, 601, DateTimeKind.Unspecified).AddTicks(5813), new DateTime(2021, 1, 23, 16, 7, 29, 441, DateTimeKind.Local).AddTicks(2287), "Theresia.Gottlieb30@gmail.com", "Kenneth", "Mosciski", new DateTime(2021, 1, 23, 16, 7, 29, 441, DateTimeKind.Local).AddTicks(2287), 18 },
                    { 8, new DateTime(1969, 2, 23, 14, 55, 57, 805, DateTimeKind.Unspecified).AddTicks(6839), new DateTime(2021, 4, 25, 22, 28, 16, 695, DateTimeKind.Local).AddTicks(4382), "Claudie57@yahoo.com", "Wanda", "Greenholt", new DateTime(2021, 4, 25, 22, 28, 16, 695, DateTimeKind.Local).AddTicks(4382), 16 },
                    { 9, new DateTime(2014, 8, 5, 0, 13, 38, 681, DateTimeKind.Unspecified).AddTicks(9408), new DateTime(2021, 3, 7, 13, 0, 50, 56, DateTimeKind.Local).AddTicks(2347), "Reggie_Roberts@hotmail.com", "Boyd", "Wolff", new DateTime(2021, 3, 7, 13, 0, 50, 56, DateTimeKind.Local).AddTicks(2347), 13 },
                    { 12, new DateTime(2005, 4, 17, 12, 45, 4, 8, DateTimeKind.Unspecified).AddTicks(7302), new DateTime(2020, 9, 30, 8, 22, 16, 331, DateTimeKind.Local).AddTicks(9967), "Lorenza_Schuppe64@hotmail.com", "Laurie", "Walter", new DateTime(2020, 9, 30, 8, 22, 16, 331, DateTimeKind.Local).AddTicks(9967), 12 },
                    { 6, new DateTime(1998, 2, 21, 23, 1, 35, 665, DateTimeKind.Unspecified).AddTicks(330), new DateTime(2021, 5, 11, 4, 25, 52, 534, DateTimeKind.Local).AddTicks(4116), "Abbigail.Rippin@hotmail.com", "Jean", "Luettgen", new DateTime(2021, 5, 11, 4, 25, 52, 534, DateTimeKind.Local).AddTicks(4116), 12 },
                    { 2, new DateTime(1985, 7, 15, 0, 12, 29, 895, DateTimeKind.Unspecified).AddTicks(3093), new DateTime(2020, 12, 20, 0, 8, 26, 141, DateTimeKind.Local).AddTicks(7833), "Verna.OReilly52@hotmail.com", "Virgil", "Kovacek", new DateTime(2020, 12, 20, 0, 8, 26, 141, DateTimeKind.Local).AddTicks(7833), 12 },
                    { 20, new DateTime(2010, 10, 13, 17, 14, 6, 58, DateTimeKind.Unspecified).AddTicks(7402), new DateTime(2020, 12, 24, 7, 26, 56, 862, DateTimeKind.Local).AddTicks(8845), "Katarina_Borer@yahoo.com", "Rufus", "Krajcik", new DateTime(2020, 12, 24, 7, 26, 56, 862, DateTimeKind.Local).AddTicks(8845), 10 },
                    { 4, new DateTime(1986, 2, 22, 6, 3, 52, 810, DateTimeKind.Unspecified).AddTicks(2944), new DateTime(2020, 9, 29, 1, 15, 52, 398, DateTimeKind.Local).AddTicks(8161), "Kara_Schumm@gmail.com", "Eloise", "Medhurst", new DateTime(2020, 9, 29, 1, 15, 52, 398, DateTimeKind.Local).AddTicks(8161), 10 },
                    { 19, new DateTime(1968, 11, 6, 6, 3, 41, 502, DateTimeKind.Unspecified).AddTicks(6510), new DateTime(2020, 9, 19, 15, 49, 51, 644, DateTimeKind.Local).AddTicks(1897), "Zander_Dickinson@gmail.com", "Erma", "Denesik", new DateTime(2020, 9, 19, 15, 49, 51, 644, DateTimeKind.Local).AddTicks(1897), 9 },
                    { 16, new DateTime(1974, 8, 31, 13, 20, 50, 844, DateTimeKind.Unspecified).AddTicks(5218), new DateTime(2020, 9, 29, 2, 29, 4, 828, DateTimeKind.Local).AddTicks(210), "Retha_Stehr99@hotmail.com", "Marcia", "Stracke", new DateTime(2020, 9, 29, 2, 29, 4, 828, DateTimeKind.Local).AddTicks(210), 8 },
                    { 15, new DateTime(1983, 7, 31, 15, 46, 25, 55, DateTimeKind.Unspecified).AddTicks(5297), new DateTime(2020, 10, 1, 23, 8, 26, 376, DateTimeKind.Local).AddTicks(4749), "Beulah6@hotmail.com", "Donald", "Hackett", new DateTime(2020, 10, 1, 23, 8, 26, 376, DateTimeKind.Local).AddTicks(4749), 8 },
                    { 13, new DateTime(1981, 2, 28, 6, 57, 52, 251, DateTimeKind.Unspecified).AddTicks(8278), new DateTime(2020, 8, 2, 11, 17, 36, 449, DateTimeKind.Local).AddTicks(699), "Vickie.Hartmann40@gmail.com", "Dean", "Sipes", new DateTime(2020, 8, 2, 11, 17, 36, 449, DateTimeKind.Local).AddTicks(699), 8 },
                    { 7, new DateTime(1971, 10, 1, 2, 43, 33, 917, DateTimeKind.Unspecified).AddTicks(4432), new DateTime(2020, 12, 11, 22, 37, 41, 521, DateTimeKind.Local).AddTicks(5842), "Yoshiko7@hotmail.com", "Bradford", "Adams", new DateTime(2020, 12, 11, 22, 37, 41, 521, DateTimeKind.Local).AddTicks(5842), 8 },
                    { 5, new DateTime(1988, 11, 15, 17, 19, 21, 624, DateTimeKind.Unspecified).AddTicks(8421), new DateTime(2021, 4, 13, 5, 27, 18, 427, DateTimeKind.Local).AddTicks(7582), "Natalia.Runte@hotmail.com", "Johanna", "Hermann", new DateTime(2021, 4, 13, 5, 27, 18, 427, DateTimeKind.Local).AddTicks(7582), 8 },
                    { 1, new DateTime(1975, 10, 31, 15, 10, 39, 973, DateTimeKind.Unspecified).AddTicks(5407), new DateTime(2021, 6, 21, 17, 26, 21, 340, DateTimeKind.Local).AddTicks(976), "Jaime.Vandervort82@gmail.com", "Genevieve", "Hyatt", new DateTime(2021, 6, 21, 17, 26, 21, 340, DateTimeKind.Local).AddTicks(976), 5 },
                    { 11, new DateTime(1998, 5, 19, 11, 42, 55, 83, DateTimeKind.Unspecified).AddTicks(1086), new DateTime(2020, 12, 12, 3, 3, 17, 330, DateTimeKind.Local).AddTicks(7494), "Emil87@yahoo.com", "Pedro", "Mraz", new DateTime(2020, 12, 12, 3, 3, 17, 330, DateTimeKind.Local).AddTicks(7494), 3 },
                    { 3, new DateTime(2005, 2, 26, 19, 11, 29, 969, DateTimeKind.Unspecified).AddTicks(2394), new DateTime(2020, 10, 17, 19, 2, 8, 700, DateTimeKind.Local).AddTicks(5577), "Talia.Larkin65@hotmail.com", "Isabel", "Hauck", new DateTime(2020, 10, 17, 19, 2, 8, 700, DateTimeKind.Local).AddTicks(5577), 2 },
                    { 10, new DateTime(2005, 11, 24, 0, 45, 39, 459, DateTimeKind.Unspecified).AddTicks(7752), new DateTime(2021, 6, 29, 17, 36, 52, 318, DateTimeKind.Local).AddTicks(5302), "Vena_Lowe@gmail.com", "Caroline", "Sawayn", new DateTime(2021, 6, 29, 17, 36, 52, 318, DateTimeKind.Local).AddTicks(5302), 19 },
                    { 14, new DateTime(2008, 6, 6, 8, 48, 5, 916, DateTimeKind.Unspecified).AddTicks(8634), new DateTime(2021, 1, 23, 3, 57, 37, 460, DateTimeKind.Local).AddTicks(9656), "Kasandra_Mann72@gmail.com", "Marie", "Robel", new DateTime(2021, 1, 23, 3, 57, 37, 460, DateTimeKind.Local).AddTicks(9656), 20 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 5, 17, new DateTime(2019, 5, 20, 4, 15, 15, 145, DateTimeKind.Unspecified).AddTicks(9452), new DateTime(2021, 11, 2, 5, 33, 7, 142, DateTimeKind.Unspecified).AddTicks(6651), "Aut harum quos commodi vel enim distinctio.", "ea", 5 },
                    { 4, 8, new DateTime(2018, 3, 23, 6, 47, 38, 629, DateTimeKind.Unspecified).AddTicks(444), new DateTime(2022, 5, 6, 12, 20, 42, 119, DateTimeKind.Unspecified).AddTicks(8655), "Sit libero voluptatem in magni quae dolor quis quis minima.", "perferendis", 6 },
                    { 9, 12, new DateTime(2017, 8, 12, 17, 33, 32, 651, DateTimeKind.Unspecified).AddTicks(6890), new DateTime(2021, 7, 24, 10, 58, 28, 112, DateTimeKind.Unspecified).AddTicks(2238), "Optio a laudantium repellat.", "rerum", 13 },
                    { 8, 12, new DateTime(2019, 12, 28, 22, 26, 8, 921, DateTimeKind.Unspecified).AddTicks(1462), new DateTime(2021, 2, 21, 9, 51, 33, 546, DateTimeKind.Unspecified).AddTicks(2210), "Hic in expedita asperiores sint ducimus eos consectetur.", "rerum", 20 },
                    { 15, 6, new DateTime(2017, 10, 29, 9, 15, 31, 440, DateTimeKind.Unspecified).AddTicks(2661), new DateTime(2020, 8, 26, 23, 37, 40, 849, DateTimeKind.Unspecified).AddTicks(8322), "Aperiam voluptate sunt autem vero ex.", "suscipit", 5 },
                    { 12, 6, new DateTime(2019, 3, 4, 7, 47, 47, 62, DateTimeKind.Unspecified).AddTicks(989), new DateTime(2020, 8, 5, 10, 13, 29, 833, DateTimeKind.Unspecified).AddTicks(2916), "Blanditiis sunt perferendis doloribus molestiae vitae corrupti aut exercitationem necessitatibus.", "quis", 9 },
                    { 19, 20, new DateTime(2018, 2, 6, 17, 52, 16, 322, DateTimeKind.Unspecified).AddTicks(3599), new DateTime(2021, 7, 23, 0, 38, 50, 478, DateTimeKind.Unspecified).AddTicks(6953), "Deleniti tenetur in et quia dolores quae.", "ipsa", 11 },
                    { 13, 20, new DateTime(2018, 5, 25, 12, 54, 39, 310, DateTimeKind.Unspecified).AddTicks(5387), new DateTime(2021, 1, 1, 11, 40, 3, 880, DateTimeKind.Unspecified).AddTicks(9268), "Vel provident beatae.", "deserunt", 13 },
                    { 18, 4, new DateTime(2020, 3, 11, 23, 15, 50, 711, DateTimeKind.Unspecified).AddTicks(5322), new DateTime(2021, 11, 17, 14, 31, 51, 593, DateTimeKind.Unspecified).AddTicks(6906), "Voluptas veniam ex cum sed et magni.", "omnis", 3 },
                    { 11, 4, new DateTime(2017, 12, 9, 0, 57, 54, 301, DateTimeKind.Unspecified).AddTicks(2534), new DateTime(2021, 4, 23, 10, 49, 1, 641, DateTimeKind.Unspecified).AddTicks(2624), "Sit exercitationem eos temporibus eum nisi.", "eligendi", 11 },
                    { 10, 19, new DateTime(2018, 11, 9, 16, 21, 29, 135, DateTimeKind.Unspecified).AddTicks(8098), new DateTime(2022, 1, 16, 4, 51, 23, 70, DateTimeKind.Unspecified).AddTicks(7458), "Cupiditate illo nihil itaque cumque repudiandae ad dicta neque quaerat.", "suscipit", 13 },
                    { 17, 15, new DateTime(2017, 8, 25, 11, 55, 54, 705, DateTimeKind.Unspecified).AddTicks(6756), new DateTime(2021, 3, 19, 3, 13, 12, 821, DateTimeKind.Unspecified).AddTicks(1728), "Enim sit fugit quo animi.", "unde", 5 },
                    { 2, 15, new DateTime(2018, 7, 25, 23, 9, 27, 226, DateTimeKind.Unspecified).AddTicks(5471), new DateTime(2020, 10, 6, 5, 21, 43, 403, DateTimeKind.Unspecified).AddTicks(5274), "Dolores nam eos est.", "quo", 9 },
                    { 7, 13, new DateTime(2019, 1, 18, 12, 10, 42, 454, DateTimeKind.Unspecified).AddTicks(9143), new DateTime(2021, 9, 1, 8, 4, 40, 917, DateTimeKind.Unspecified).AddTicks(8579), "Voluptas aspernatur nihil veniam architecto.", "officia", 8 },
                    { 6, 13, new DateTime(2018, 8, 11, 3, 25, 40, 809, DateTimeKind.Unspecified).AddTicks(3108), new DateTime(2021, 1, 5, 22, 13, 4, 807, DateTimeKind.Unspecified).AddTicks(2388), "Non minus porro aut expedita.", "asperiores", 13 },
                    { 3, 7, new DateTime(2017, 10, 16, 19, 19, 20, 561, DateTimeKind.Unspecified).AddTicks(7788), new DateTime(2021, 1, 11, 5, 13, 17, 881, DateTimeKind.Unspecified).AddTicks(1847), "Voluptas accusantium quod id aut porro dolores debitis.", "corrupti", 2 },
                    { 20, 5, new DateTime(2017, 12, 30, 6, 31, 44, 42, DateTimeKind.Unspecified).AddTicks(1074), new DateTime(2021, 4, 6, 11, 33, 53, 518, DateTimeKind.Unspecified).AddTicks(8764), "Est et dolorem enim tenetur nisi vitae et sit aut.", "ea", 5 },
                    { 14, 3, new DateTime(2018, 11, 22, 17, 56, 49, 401, DateTimeKind.Unspecified).AddTicks(7479), new DateTime(2020, 9, 24, 4, 27, 51, 827, DateTimeKind.Unspecified).AddTicks(3831), "Aut ea culpa et possimus beatae eius et.", "dolore", 13 },
                    { 1, 18, new DateTime(2017, 12, 12, 11, 16, 48, 296, DateTimeKind.Unspecified).AddTicks(5548), new DateTime(2020, 11, 10, 15, 52, 20, 703, DateTimeKind.Unspecified).AddTicks(3884), "Molestiae et tempora repellat id ad ipsam qui iure sed.", "debitis", 16 },
                    { 16, 10, new DateTime(2019, 5, 27, 3, 16, 46, 984, DateTimeKind.Unspecified).AddTicks(8473), new DateTime(2022, 5, 8, 22, 38, 38, 173, DateTimeKind.Unspecified).AddTicks(8999), "Nulla illum saepe nulla repellendus.", "quis", 10 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 5, new DateTime(2020, 9, 15, 13, 4, 6, 841, DateTimeKind.Unspecified).AddTicks(1564), "Et quis voluptate ipsam saepe ut est.", new DateTime(2020, 11, 16, 12, 8, 53, 835, DateTimeKind.Unspecified).AddTicks(6391), "Odit nihil nihil repudiandae.", 1, 5, 0 },
                    { 1, new DateTime(2019, 12, 15, 5, 55, 30, 375, DateTimeKind.Unspecified).AddTicks(790), "Vel aliquam qui quas est id.", new DateTime(2021, 1, 2, 14, 41, 30, 115, DateTimeKind.Unspecified).AddTicks(8775), "Est doloribus aut quasi.", 4, 16, 0 },
                    { 12, new DateTime(2020, 7, 18, 7, 20, 22, 488, DateTimeKind.Unspecified).AddTicks(3161), "Consequatur fugit eum doloremque omnis sapiente dolores aut quas autem.", new DateTime(2020, 7, 19, 9, 16, 8, 327, DateTimeKind.Unspecified).AddTicks(4978), "Assumenda vero fugit.", 19, 12, 1 },
                    { 9, new DateTime(2020, 7, 6, 4, 33, 33, 65, DateTimeKind.Unspecified).AddTicks(7187), "Veritatis quia nesciunt eligendi autem cumque et veritatis.", new DateTime(2020, 12, 25, 17, 14, 4, 925, DateTimeKind.Unspecified).AddTicks(4835), "Officiis assumenda illum ipsum quasi veritatis alias.", 9, 13, 3 },
                    { 4, new DateTime(2019, 9, 14, 15, 57, 24, 117, DateTimeKind.Unspecified).AddTicks(7616), "Ut molestiae quia unde voluptas deleniti omnis repudiandae.", new DateTime(2019, 10, 28, 10, 11, 46, 575, DateTimeKind.Unspecified).AddTicks(8529), "Libero voluptatibus ratione minus non.", 9, 13, 2 },
                    { 14, new DateTime(2020, 11, 21, 4, 13, 18, 126, DateTimeKind.Unspecified).AddTicks(5190), "Minus provident impedit et et nobis aut sunt vero.", new DateTime(2021, 3, 20, 6, 43, 8, 29, DateTimeKind.Unspecified).AddTicks(7755), "Odio nulla beatae at est est doloremque reprehenderit.", 9, 10, 0 },
                    { 11, new DateTime(2019, 4, 24, 16, 15, 47, 205, DateTimeKind.Unspecified).AddTicks(654), "Aut ea id maxime eos saepe quaerat non.", new DateTime(2021, 2, 14, 6, 43, 51, 56, DateTimeKind.Unspecified).AddTicks(5787), "Odit corporis corporis totam libero sunt sunt nemo neque deleniti.", 9, 10, 2 },
                    { 2, new DateTime(2021, 2, 12, 9, 59, 31, 895, DateTimeKind.Unspecified).AddTicks(8764), "Molestias reiciendis itaque est in odit.", new DateTime(2021, 2, 18, 20, 51, 42, 652, DateTimeKind.Unspecified).AddTicks(9865), "Alias labore cumque est.", 9, 10, 3 },
                    { 13, new DateTime(2017, 10, 29, 13, 43, 17, 118, DateTimeKind.Unspecified).AddTicks(4857), "Cupiditate suscipit et ducimus eveniet.", new DateTime(2018, 9, 9, 18, 39, 23, 754, DateTimeKind.Unspecified).AddTicks(2275), "Consequatur dicta aut ut consequatur eligendi qui rerum.", 1, 17, 2 },
                    { 20, new DateTime(2018, 10, 5, 20, 8, 28, 137, DateTimeKind.Unspecified).AddTicks(4264), "Vel et sint quia eos possimus a veniam sunt quisquam.", new DateTime(2019, 9, 11, 4, 10, 58, 504, DateTimeKind.Unspecified).AddTicks(6192), "Quia tempora et praesentium.", 19, 2, 3 },
                    { 3, new DateTime(2020, 8, 30, 23, 54, 53, 546, DateTimeKind.Unspecified).AddTicks(6597), "Fugiat ut optio adipisci earum placeat rerum id ut.", new DateTime(2020, 9, 13, 17, 5, 34, 43, DateTimeKind.Unspecified).AddTicks(6495), "Provident quas rem et cupiditate laudantium temporibus perferendis modi voluptas.", 9, 6, 0 },
                    { 18, new DateTime(2018, 4, 4, 16, 56, 14, 826, DateTimeKind.Unspecified).AddTicks(5566), "Exercitationem fuga deleniti exercitationem velit est impedit.", new DateTime(2020, 11, 10, 17, 30, 5, 168, DateTimeKind.Unspecified).AddTicks(43), "Dolorem aperiam voluptate libero eos aut.", 1, 20, 0 },
                    { 16, new DateTime(2020, 1, 11, 11, 11, 10, 619, DateTimeKind.Unspecified).AddTicks(3311), "Dolorem ratione quisquam consequatur repellendus tempora.", new DateTime(2020, 10, 23, 8, 8, 18, 149, DateTimeKind.Unspecified).AddTicks(8710), "Et consequatur autem non fugiat rerum dolor aspernatur ducimus non.", 1, 20, 3 },
                    { 8, new DateTime(2019, 1, 17, 19, 17, 34, 349, DateTimeKind.Unspecified).AddTicks(5607), "Unde ut aliquid qui dolorum eveniet alias unde.", new DateTime(2019, 11, 25, 5, 26, 25, 746, DateTimeKind.Unspecified).AddTicks(1839), "Numquam ut odio.", 9, 14, 2 },
                    { 19, new DateTime(2020, 8, 11, 1, 6, 48, 58, DateTimeKind.Unspecified).AddTicks(5672), "Ratione rerum et reprehenderit in debitis qui explicabo doloribus.", new DateTime(2021, 4, 20, 5, 10, 42, 249, DateTimeKind.Unspecified).AddTicks(1301), "Provident quasi beatae quia in esse reiciendis porro.", 1, 5, 1 },
                    { 15, new DateTime(2019, 12, 14, 14, 44, 41, 71, DateTimeKind.Unspecified).AddTicks(3327), "Amet occaecati consequatur quia assumenda velit nam.", new DateTime(2020, 8, 29, 22, 33, 2, 117, DateTimeKind.Unspecified).AddTicks(4464), "Doloremque fuga necessitatibus distinctio id distinctio sint amet.", 1, 5, 0 },
                    { 10, new DateTime(2020, 5, 4, 3, 20, 0, 810, DateTimeKind.Unspecified).AddTicks(8311), "Hic magni non id sit ipsa velit vel ad iste.", new DateTime(2020, 9, 3, 2, 12, 54, 506, DateTimeKind.Unspecified).AddTicks(7250), "Quis quaerat quos a expedita ut odio in.", 1, 5, 3 },
                    { 7, new DateTime(2019, 12, 25, 16, 14, 48, 465, DateTimeKind.Unspecified).AddTicks(196), "Ipsam in omnis ut maiores odit aut.", new DateTime(2020, 7, 1, 1, 20, 34, 874, DateTimeKind.Unspecified).AddTicks(5078), "Cum nam sapiente quaerat.", 1, 5, 3 },
                    { 6, new DateTime(2021, 10, 13, 15, 2, 8, 843, DateTimeKind.Unspecified).AddTicks(2649), "Ut ipsum qui et delectus quasi aliquid.", new DateTime(2022, 4, 11, 15, 43, 25, 509, DateTimeKind.Unspecified).AddTicks(4588), "Et quos magni animi nihil corrupti.", 20, 16, 3 },
                    { 17, new DateTime(2019, 10, 10, 6, 15, 0, 469, DateTimeKind.Unspecified).AddTicks(7844), "Ut facere doloremque.", new DateTime(2020, 5, 30, 23, 56, 27, 879, DateTimeKind.Unspecified).AddTicks(8727), "Cum omnis aut expedita.", 20, 16, 0 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 19);
        }
    }
}
