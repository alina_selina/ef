﻿using System;

namespace CollectionsAndLINQ.Entities
{
    public class User : Entity
    {
        public int? TeamId { get; set; }
        public Team Team { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get => CreatedAt; set => CreatedAt = value; }
        public DateTime BirthDay { get; set; }

        public override string ToString()
        {
            return $"UserId: {Id}, TeamId: {TeamId}, FirstName: {FirstName}, LastName: {LastName}, Email: {Email}, BirthDay: {BirthDay}";
        }
    }
}
