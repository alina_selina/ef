﻿using System.Collections.Generic;
using System.Threading;

namespace CollectionsAndLINQ.Entities
{
    public class Team : Entity
    {
        public string Name { get; set; }
        public ICollection<User> Users { get; set; }

        public Team()
        {
            Users = new List<User>();
        }

        public override string ToString()
        {
            return $"Team id: {Id}, Name: {Name}, CreatedAt: {CreatedAt}";
        }
    }
}
