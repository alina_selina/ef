﻿using Projects.Common.Interfaces;
using System;

namespace CollectionsAndLINQ.Entities
{
    public abstract class Entity : IEntity
    {
        private DateTime _createdAt;

        public virtual DateTime CreatedAt
        {
            get => _createdAt;
            set => _createdAt = (value == DateTime.MinValue) ? DateTime.Now : value;
        }

        public int Id { get; set; }

        public Entity()
        {
            CreatedAt = DateTime.Now;
        }
    }
}
