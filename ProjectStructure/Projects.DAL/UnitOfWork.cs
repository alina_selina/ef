﻿using AutoMapper;
using Projects.Common.Interfaces;
using Projects.DAL.Data;
using Projects.DAL.Interfaces;
using Projects.DAL.Interfaces.Repositories;
using Projects.DAL.Repositories;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Projects.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDictionary<Type, object> Repositories { get; }
        private readonly ProjectsContext _context;

        public UnitOfWork(ProjectsContext context)
        {
            _context = context;
            Repositories = new ConcurrentDictionary<Type, object>();
        }

        public void Dispose()
        {
            Repositories?.Clear();
            _context?.Dispose();
        }

        public IRepository<TEntity> GetRepository<TEntity>() where TEntity : class, IEntity
        {
            Type entityType = typeof(TEntity);

            if (Repositories.ContainsKey(entityType))
                return (IRepository<TEntity>)Repositories[entityType];

            Repositories[entityType] = new Repository<TEntity>(_context);
            return (IRepository<TEntity>)Repositories[entityType];
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
