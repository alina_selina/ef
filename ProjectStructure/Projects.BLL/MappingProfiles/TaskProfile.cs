﻿using AutoMapper;
using CollectionsAndLINQ.Entities;
using Projects.Common.DTO.Task;
using System;

namespace Projects.BLL.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<TaskCreateDTO, Task>()
                .ForMember(dest => dest.CreatedAt, src => src.MapFrom(s => s.CreatedAt ?? DateTime.Now));

            CreateMap<TaskDTO, Task>();
            CreateMap<Task, TaskDTO>();
        }
    }
}
