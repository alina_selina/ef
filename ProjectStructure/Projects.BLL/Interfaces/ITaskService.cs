﻿using Projects.Common.DTO.Task;

namespace Projects.BLL.Interfaces
{
    public interface ITaskService : IService<TaskDTO, TaskCreateDTO>
    {
    }
}
