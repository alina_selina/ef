﻿using Projects.Common.DTO;

namespace Projects.BLL.Interfaces
{
    public interface IProjectService : IService<ProjectDTO, ProjectCreateDTO>
    {
    }
}
