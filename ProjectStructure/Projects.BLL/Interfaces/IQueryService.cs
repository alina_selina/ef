﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Projects.Common.DTO.Query;
using Projects.Common.DTO.Task;

namespace Projects.BLL.Interfaces
{
    public interface IQueryService
    {
        Task<IEnumerable<ProjectTasksDTO>> GetProjectTaskCountByUserIdAsync(int userId);

        Task<IEnumerable<TaskDTO>> GetUserTasksByUserIdAsync(int userId);

        Task<IEnumerable<UserTasksCurrentYearDTO>> GetUserTasksCurrentYearByUserIdAsync(int userId);

        Task<IEnumerable<TeamsDTO>> GetTeamsAsync();

        Task<IEnumerable<UsersDTO>> GetUsersAsync();

        Task<UserStructDTO> GetUserStructAsync(int userId);

        Task<ProjectStructDTO> GetProjectStructAsync(int projectId);

        Task<IEnumerable<TaskDTO>> GetUnfinishedTasksByUserIdAsync(int userId);
    }
}
