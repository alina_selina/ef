﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Projects.BLL.Interfaces
{
    public interface IService<TDto, TCreateDto> where TDto : class where TCreateDto : class
    {
        Task<IEnumerable<TDto>> GetAllAsync();

        Task<TDto> GetByIdAsync(object id);

        Task<TDto> CreateAsync(TCreateDto dto);

        Task<TDto> UpdateAsync(TDto dto);

        Task DeleteAsync(object id);
    }
}
