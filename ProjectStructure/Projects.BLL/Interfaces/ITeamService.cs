﻿using Projects.Common.DTO.Team;

namespace Projects.BLL.Interfaces
{
    public interface ITeamService : IService<TeamDTO, TeamCreateDTO>
    {
    }
}
