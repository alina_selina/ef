﻿using System;

namespace Projects.BLL.Exceptions
{
    public class SaveException : Exception
    {
        public SaveException() : base("Save exception occurred.")
        {
        }

        public SaveException(string message) : base(message)
        {
        }
    }
}
