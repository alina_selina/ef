﻿using System;

namespace Projects.BLL.Exceptions
{
    public class InvalidArgumentException : Exception
    {
        public InvalidArgumentException() : base("Invalid argument passed.")
        {
        }

        public InvalidArgumentException(string message) : base(message)
        {
        }
    }
}
