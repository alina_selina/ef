﻿using AutoMapper;
using CollectionsAndLINQ.Entities;
using Projects.BLL.Interfaces;
using Projects.BLL.Exceptions;
using Projects.BLL.Services.Abstract;
using Projects.Common.DTO;
using Projects.DAL.Interfaces;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Projects.Common.Interfaces;
using System.Threading.Tasks;

namespace Projects.BLL.Services
{
    public class ProjectService : Service<ProjectDTO, ProjectCreateDTO>, IProjectService
    {
        public ProjectService(IUnitOfWork uow, IMapper mapper) : base(uow, mapper)
        {
        }

        public override async Task<ProjectDTO> CreateAsync(ProjectCreateDTO dto)
        {
            var project = await GetProjectAsync(dto);

            await _uow.GetRepository<Project>().CreateAsync(project);

            var result = await _uow.SaveChangesAsync();
            if (result == 0)
                throw new SaveException();

            return _mapper.Map<ProjectDTO>(project);
        }

        public override async Task<ProjectDTO> UpdateAsync(ProjectDTO dto)
        {
            var project = await GetProjectAsync(dto);

            var updatedProject = await _uow.GetRepository<Project>().UpdateAsync(project);
            await _uow.SaveChangesAsync();

            if (updatedProject == null)
                throw new NotFoundException(nameof(Project), project.Id.ToString());

            return _mapper.Map<ProjectDTO>(updatedProject);
        }

        private async Task<Project> GetProjectAsync(IProject dto)
        {
            var author = await _uow.GetRepository<User>().GetFirstOrDefaultAsync(u => u.Id == dto.AuthorId);

            if (author == null)
                throw new InvalidArgumentException($"{nameof(User)} with id {dto.AuthorId} was not found");

            var team = await _uow.GetRepository<Team>().GetFirstOrDefaultAsync(t => t.Id == dto.TeamId);

            if (team == null)
                throw new InvalidArgumentException($"{nameof(Team)} with id {dto.TeamId} was not found");

            return _mapper.Map<Project>(dto);
        }

        public override async Task<IEnumerable<ProjectDTO>> GetAllAsync()
        {
            var projects = await _uow.GetRepository<Project>().GetAllAsync(pr => pr.Include(p => p.Author)
                                                                                   .Include(p => p.Team), true);

            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }

        public override async System.Threading.Tasks.Task DeleteAsync(object id)
        {
            var project = await _uow.GetRepository<Project>().GetFirstOrDefaultAsync(p => p.Id.Equals(id));

            if (project == null)
                throw new NotFoundException(nameof(Project), id.ToString());

            await _uow.GetRepository<Project>().DeleteAsync(id);
            await _uow.SaveChangesAsync();
        }

        public override async Task<ProjectDTO> GetByIdAsync(object id)
        {
            var project = await _uow.GetRepository<Project>().GetFirstOrDefaultAsync(u => u.Id.Equals(id),
                                                                                    pr => pr.Include(p => p.Author)
                                                                                            .Include(p => p.Team), true);

            if (project == null)
                throw new NotFoundException(nameof(Project), id.ToString());

            return _mapper.Map<ProjectDTO>(project);
        }
    }
}
