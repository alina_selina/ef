﻿using AutoMapper;
using CollectionsAndLINQ.Entities;
using Projects.BLL.Interfaces;
using Projects.BLL.Exceptions;
using Projects.BLL.Services.Abstract;
using Projects.Common.DTO.Task;
using Projects.Common.Enums;
using Projects.Common.Interfaces;
using Projects.DAL.Interfaces;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;
using System.Threading.Tasks;
using ProjectTask = CollectionsAndLINQ.Entities.Task;

namespace Projects.BLL.Services
{
    public class TaskService : Service<TaskDTO, TaskCreateDTO>, ITaskService
    {
        public TaskService(IUnitOfWork uow, IMapper mapper) : base(uow, mapper)
        {
        }

        public override async Task<TaskDTO> CreateAsync(TaskCreateDTO dto)
        {
            var task = await GetTaskAsync(dto);

            await _uow.GetRepository<ProjectTask>().CreateAsync(task);

            var result = await _uow.SaveChangesAsync();
            if (result == 0)
                throw new SaveException();

            return _mapper.Map<TaskDTO>(task);
        }

        public override async Task<TaskDTO> UpdateAsync(TaskDTO dto)
        {
            var task = await GetTaskAsync(dto);

            var updatedTask = await _uow.GetRepository<ProjectTask>().UpdateAsync(task);
            await _uow.SaveChangesAsync();

            if (updatedTask == null)
                throw new NotFoundException(nameof(ProjectTask), task.Id.ToString());

            return _mapper.Map<TaskDTO>(updatedTask);
        }

        private async Task<ProjectTask> GetTaskAsync(ITask dto)
        {
            if (!Enum.IsDefined(dto.State))
                throw new InvalidArgumentException($"{nameof(TaskState)} with id {dto.State} was not found");

            var project = await _uow.GetRepository<Project>().GetFirstOrDefaultAsync(p => p.Id == dto.ProjectId,
                                                                                     pr => pr.Include(p => p.Team)
                                                                                             .ThenInclude(t => t.Users));
            if (project == null)
                throw new InvalidArgumentException($"{nameof(Project)} with id {dto.ProjectId} was not found");

            var user = project.Team.Users.FirstOrDefault(u => u.Id == dto.PerformerId);

            if (user == null)
                throw new InvalidArgumentException($"{nameof(User)} with id {dto.PerformerId} was not found");

            return _mapper.Map<ProjectTask>(dto);
        }

        public override async Task<IEnumerable<TaskDTO>> GetAllAsync()
        {
            var tasks = await _uow.GetRepository<ProjectTask>().GetAllAsync(tr => tr.Include(t => t.Performer)
                                                                                    .Include(t => t.Project), true);

            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }

        public override async System.Threading.Tasks.Task DeleteAsync(object id)
        {
            var task = await _uow.GetRepository<ProjectTask>().GetFirstOrDefaultAsync(t => t.Id.Equals(id));

            if (task == null)
                throw new NotFoundException(nameof(ProjectTask), id.ToString());

            await _uow.GetRepository<ProjectTask>().DeleteAsync(id);
            await _uow.SaveChangesAsync();
        }

        public override async Task<TaskDTO> GetByIdAsync(object id)
        {
            var task = await _uow.GetRepository<ProjectTask>().GetFirstOrDefaultAsync(u => u.Id.Equals(id),
                                                                                     tr => tr.Include(t => t.Performer)
                                                                                             .Include(t => t.Project), true);
            if (task == null)
                throw new NotFoundException(nameof(ProjectTask), id.ToString());

            return _mapper.Map<TaskDTO>(task);
        }
    }
}
