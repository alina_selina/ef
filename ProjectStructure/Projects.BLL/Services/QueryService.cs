﻿using AutoMapper;
using CollectionsAndLINQ.Entities;
using Microsoft.EntityFrameworkCore;
using Projects.BLL.Exceptions;
using Projects.BLL.Interfaces;
using Projects.BLL.Services.Abstract;
using Projects.Common.DTO;
using Projects.Common.DTO.Query;
using Projects.Common.DTO.Task;
using Projects.Common.DTO.User;
using Projects.Common.Enums;
using Projects.Common.Interfaces;
using Projects.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectTask = CollectionsAndLINQ.Entities.Task;
using Task = System.Threading.Tasks.Task;

namespace Projects.BLL.Services
{
    public class QueryService : BaseService, IQueryService
    {
        public QueryService(IUnitOfWork provider, IMapper mapper) : base(provider, mapper)
        {
        }

        private async Task<ICollection<Project>> GetProjects() => await _uow.GetRepository<Project>()
                                                                            .GetAllAsync(pr => pr.Include(p => p.Author)
                                                                                                 .Include(p => p.Team)
                                                                                                 .ThenInclude(t => t.Users)
                                                                                                 .Include(p => p.Tasks)
                                                                                                 .ThenInclude(t => t.Performer));

        private async Task<ICollection<ProjectTask>> GetTasks() => await _uow.GetRepository<ProjectTask>()
                                                                             .GetAllAsync(tr => tr.Include(t => t.Performer)
                                                                                                  .Include(t => t.Project));

        private async Task<ICollection<Team>> GetTeams() => await _uow.GetRepository<Team>()
                                                                      .GetAllAsync(tr => tr.Include(t => t.Users));


        /*Получить кол-во тасков у проекта конкретного пользователя (по id) (словарь проект:кол-во тасков).*/
        public async Task<IEnumerable<ProjectTasksDTO>> GetProjectTaskCountByUserIdAsync(int userId)
        {
            if (!await TryGetEntityByIdAsync<User>(userId))
                throw new InvalidArgumentException($"{nameof(User)} with id {userId} was not found");

            var projects = await GetProjects();

            return projects.SelectMany(p => p.Tasks, (project, tasks) => new { project, tasks })
                .Where(projectTasks => projectTasks.tasks.Performer.Id == userId)
                .Select(p => new ProjectTasksDTO()
                {
                    Project = _mapper.Map<ProjectDTO>(p.project),
                    TaskCount = p.project.Tasks.Count
                })
                .Distinct();
        }

        /*Получить список тасков, назначенных на конкретного пользователя (по id), где name таска < 45 символов (коллекция тасков).*/
        public async Task<IEnumerable<TaskDTO>> GetUserTasksByUserIdAsync(int userId)
        {
            if (!await TryGetEntityByIdAsync<User>(userId))
                throw new InvalidArgumentException($"{nameof(User)} with id {userId} was not found");

            var tasks = await GetTasks();

            return _mapper.Map<List<TaskDTO>>(tasks.Where(t => t.Performer.Id == userId && t.Name.Length < 45));
        }

        /*Получить список(id, name) из коллекции тасков, которые выполнены(finished) в текущем(2021) году для конкрет. пользователя (по id).*/
        public async Task<IEnumerable<UserTasksCurrentYearDTO>> GetUserTasksCurrentYearByUserIdAsync(int userId)
        {
            if (!await TryGetEntityByIdAsync<User>(userId))
                throw new InvalidArgumentException($"{nameof(User)} with id {userId} was not found");

            var tasks = await GetTasks();

            return tasks.Where(t => t.Performer.Id == userId && t.FinishedAt?.Year == DateTime.Now.Year)
                .Select(t => new UserTasksCurrentYearDTO()
                {
                    Id = t.Id,
                    Name = t.Name
                });
        }

        /* Получить список (id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет, 
         * отсортированных по дате регистрации пользователя по убыванию, а также сгруппированных по командам.*/
        public async Task<IEnumerable<TeamsDTO>> GetTeamsAsync()
        {
            var teams = await GetTeams();

            return teams.Where(t => t.Users?.Count > 0 && t.Users.All(u => (DateTime.Now.Year - u.BirthDay.Year) > 10))
                .Select(t => new TeamsDTO()
                {
                    Id = t.Id,
                    Name = t.Name,
                    Users = _mapper.Map<List<UserDTO>>(t.Users.OrderByDescending(u => u.RegisteredAt).ToList())
                });
        }

        /*Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию).*/
        public async Task<IEnumerable<UsersDTO>> GetUsersAsync()
        {
            var tasks = await GetTasks();

            return tasks.GroupBy(u => u.Performer.Id, t => new { Task = t, t.Performer })
                .Select(group => new UsersDTO()
                {
                    User = _mapper.Map<UserDTO>(group.Select(g => g.Performer).First()),
                    Tasks = _mapper.Map<List<TaskDTO>>(group.OrderByDescending(u => u.Task.Name.Length).Select(g => g.Task).ToList())
                })
                .OrderBy(u => u.User.FirstName);
        }

        /*Получить следующую структуру (передать Id пользователя в параметры):
            User
            Последний проект пользователя (по дате создания)
            Общее кол-во тасков под последним проектом
            Общее кол-во незавершенных или отмененных тасков для пользователя
            Самый долгий таск пользователя по дате (раньше всего создан - позже всего закончен)*/
        public async Task<UserStructDTO> GetUserStructAsync(int userId)
        {
            if (!await TryGetEntityByIdAsync<User>(userId))
                throw new InvalidArgumentException($"{nameof(User)} with id {userId} was not found");

            var projects = await GetProjects();

            var lastProjectTask = Task.Run(() => projects.SelectMany(p => p.Team.Users.Where(t => t.Id == userId), (project, users) => new { project })
                                                .OrderByDescending(p => p.project.CreatedAt)
                                                .FirstOrDefault().project);

            var tasksTask = Task.Run(() => projects.SelectMany(p => p.Tasks.Where(t => t.Performer.Id == userId)));

            await Task.WhenAll(lastProjectTask, tasksTask);

            return (from p in projects
                    from t in p.Tasks
                    where t.Performer.Id == userId
                    select new UserStructDTO()
                    {
                        User = _mapper.Map<UserDTO>(t.Performer),
                        LastProject = _mapper.Map<ProjectDTO>(lastProjectTask.Result),
                        LastProjectTaskCount = lastProjectTask.Result.Tasks.Count,
                        UnfinishedTaskCount = tasksTask.Result.Where(t => t.State == TaskState.InProgress || t.State == TaskState.Canceled).Count(),
                        LongestTask = _mapper.Map<TaskDTO>(tasksTask.Result.OrderByDescending(t => (t.FinishedAt ?? DateTime.Now) - t.CreatedAt).FirstOrDefault())
                    })
                    .FirstOrDefault();
        }

        /*Получить следующую структуру:
            Проект
            Самый длинный таск проекта (по описанию)
            Самый короткий таск проекта (по имени)
            Общее кол-во пользователей в команде проекта, где или описание проекта > 20 символов или кол-во тасков < 3*/
        public async Task<ProjectStructDTO> GetProjectStructAsync(int projectId)
        {
            if (!await TryGetEntityByIdAsync<Project>(projectId))
                throw new InvalidArgumentException($"{nameof(Project)} with id {projectId} was not found");

            var projects = await GetProjects();

            return projects.Where(p => p.Id == projectId)
                .Select(p => new ProjectStructDTO()
                {
                    Project = _mapper.Map<ProjectDTO>(p),
                    LongestTask = _mapper.Map<TaskDTO>(p.Tasks.OrderByDescending(t => t.Description.Length).FirstOrDefault()),
                    ShortestTask = _mapper.Map<TaskDTO>(p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault()),
                    UserCount = (p.Description.Length > 20 || p.Tasks.Count < 3) ? p.Team.Users.Count : null
                })
                .FirstOrDefault();
        }

        //возвращает выборку всех невыполненных заданий для пользователя (из всех проектов)
        public async Task<IEnumerable<TaskDTO>> GetUnfinishedTasksByUserIdAsync(int userId)
        {
            if (!await TryGetEntityByIdAsync<User>(userId))
                throw new InvalidArgumentException($"{nameof(User)} with id {userId} was not found");

            var tasks = await GetTasks();

            return _mapper.Map<List<TaskDTO>>(tasks.Where(t => t.Performer.Id == userId && t.FinishedAt == null));
        }

        private async Task<bool> TryGetEntityByIdAsync<T>(int id) where T : class, IEntity
        {
            var entity = await _uow.GetRepository<T>().GetFirstOrDefaultAsync(u => u.Id == id, disableTracking: true);
            if (entity == null)
                return false;

            return true;
        }
    }
}
