﻿using AutoMapper;
using CollectionsAndLINQ.Entities;
using Microsoft.EntityFrameworkCore;
using Projects.BLL.Exceptions;
using Projects.BLL.Interfaces;
using Projects.BLL.Services.Abstract;
using Projects.Common.DTO;
using Projects.Common.DTO.User;
using Projects.Common.Interfaces;
using Projects.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Projects.BLL.Services
{
    public class UserService : Service<UserDTO, UserCreateDTO>, IUserService
    {
        public UserService(IUnitOfWork uow, IMapper mapper) : base(uow, mapper)
        {
        }

        public override async Task<UserDTO> CreateAsync(UserCreateDTO dto)
        {
            var user = await GetUserAsync(dto);

            await _uow.GetRepository<User>().CreateAsync(user);

            var result = await _uow.SaveChangesAsync();
            if (result == 0)
                throw new SaveException();

            return _mapper.Map<UserDTO>(user);
        }

        public override async Task<UserDTO> UpdateAsync(UserDTO dto)
        {
            var user = await GetUserAsync(dto);

            var updatedUser = await _uow.GetRepository<User>().UpdateAsync(user);
            await _uow.SaveChangesAsync();

            if (updatedUser == null)
                throw new NotFoundException(nameof(User), user.Id.ToString());

            return _mapper.Map<UserDTO>(updatedUser);
        }

        private async Task<User> GetUserAsync(IUser dto)
        {
            if (dto.TeamId != null)
            {
                var team = await _uow.GetRepository<Team>().GetFirstOrDefaultAsync(t => t.Id == dto.TeamId);

                if (team == null)
                    throw new InvalidArgumentException($"{nameof(Team)} with id {dto.TeamId} was not found");
            }

            return _mapper.Map<User>(dto);
        }

        public override async Task<IEnumerable<UserDTO>> GetAllAsync()
        {
            var users = await _uow.GetRepository<User>().GetAllAsync(ur => ur.Include(u => u.Team), true);

            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }

        public override async System.Threading.Tasks.Task DeleteAsync(object id)
        {
            var user = await _uow.GetRepository<User>().GetFirstOrDefaultAsync(u => u.Id.Equals(id));

            if (user == null)
                throw new NotFoundException(nameof(User), id.ToString());

            await _uow.GetRepository<User>().DeleteAsync(id);
            await _uow.SaveChangesAsync();
        }

        public override async Task<UserDTO> GetByIdAsync(object id)
        {
            var user = await _uow.GetRepository<User>().GetFirstOrDefaultAsync(u => u.Id.Equals(id),
                                                                               ur => ur.Include(u => u.Team), true);

            if (user == null)
                throw new NotFoundException(nameof(User), id.ToString());

            return _mapper.Map<UserDTO>(user);
        }
    }
}
