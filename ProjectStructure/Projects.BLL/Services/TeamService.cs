﻿using AutoMapper;
using CollectionsAndLINQ.Entities;
using Projects.BLL.Interfaces;
using Projects.BLL.Exceptions;
using Projects.BLL.Services.Abstract;
using Projects.Common.DTO.Team;
using Projects.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Projects.BLL.Services
{
    public class TeamService : Service<TeamDTO, TeamCreateDTO>, ITeamService
    {
        public TeamService(IUnitOfWork uow, IMapper mapper) : base(uow, mapper)
        {
        }

        public override async Task<TeamDTO> CreateAsync(TeamCreateDTO dto)
        {
            var team = _mapper.Map<Team>(dto);

            await _uow.GetRepository<Team>().CreateAsync(team);

            var result = await _uow.SaveChangesAsync();
            if (result == 0)
                throw new SaveException();

            return _mapper.Map<TeamDTO>(team);
        }

        public override async Task<TeamDTO> UpdateAsync(TeamDTO dto)
        {
            var team = _mapper.Map<Team>(dto);

            var updatedTeam = await _uow.GetRepository<Team>().UpdateAsync(team);
            await _uow.SaveChangesAsync();

            if (updatedTeam == null)
                throw new NotFoundException(nameof(Team), team.Id.ToString());

            return _mapper.Map<TeamDTO>(updatedTeam);
        }

        public override async Task<IEnumerable<TeamDTO>> GetAllAsync()
        {
            var teams = await _uow.GetRepository<Team>().GetAllAsync(disableTracking: true);

            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }

        public override async System.Threading.Tasks.Task DeleteAsync(object id)
        {
            var team = await _uow.GetRepository<Team>().GetFirstOrDefaultAsync(t => t.Id.Equals(id));

            if (team == null)
                throw new NotFoundException(nameof(Team), id.ToString());

            await _uow.GetRepository<Team>().DeleteAsync(id);
            await _uow.SaveChangesAsync();
        }

        public override async Task<TeamDTO> GetByIdAsync(object id)
        {
            var team = await _uow.GetRepository<Team>().GetFirstOrDefaultAsync(u => u.Id.Equals(id), disableTracking:true);

            if (team == null)
                throw new NotFoundException(nameof(Team), id.ToString());

            return _mapper.Map<TeamDTO>(team);
        }
    }
}
