﻿using AutoMapper;
using Projects.BLL.Interfaces;
using Projects.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Projects.BLL.Services.Abstract
{
    public abstract class Service<TDto, TCreateDto> : BaseService,
                                                      IService<TDto, TCreateDto> where TDto : class where TCreateDto : class
    {
        public Service(IUnitOfWork uow, IMapper mapper) : base(uow, mapper)
        {
        }

        public abstract Task<TDto> CreateAsync(TCreateDto dto);

        public abstract Task DeleteAsync(object id);

        public abstract Task<IEnumerable<TDto>> GetAllAsync();

        public abstract Task<TDto> GetByIdAsync(object id);

        public abstract Task<TDto> UpdateAsync(TDto dto);
    }
}
