﻿using Microsoft.AspNetCore.Http;
using Projects.BLL.Exceptions;
using Projects.WebAPI.Models;
using System;
using System.Threading.Tasks;

namespace Projects.WebAPI.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception e)
            {
                await HandleErrorAsync(context, e);
            }
        }

        private async Task HandleErrorAsync(HttpContext context, Exception exception)
        {
            var errorResponse = new HttpErrorResponse();

            if (exception is HttpException httpException)
            {
                errorResponse.StatusCode = httpException.StatusCode;
                errorResponse.Message = httpException.Message;
            }

            else if (exception is InvalidArgumentException invalidArgumentException)
            {
                errorResponse.StatusCode = System.Net.HttpStatusCode.BadRequest;
                errorResponse.Message = invalidArgumentException.Message;
            }

            else if (exception is NotFoundException notFoundException)
            {
                errorResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                errorResponse.Message = notFoundException.Message;
            }

            context.Response.StatusCode = (int)errorResponse.StatusCode;
            await context.Response.WriteAsJsonAsync(errorResponse);
        }
    }
}
