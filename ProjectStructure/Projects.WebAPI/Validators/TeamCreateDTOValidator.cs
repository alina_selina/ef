﻿using FluentValidation;
using Projects.Common.DTO.Team;

namespace Projects.WebAPI.Validators
{
    public class TeamCreateDTOValidator : AbstractValidator<TeamCreateDTO>
    {
        public TeamCreateDTOValidator()
        {
            RuleFor(t => t.Name)
                .NotEmpty().WithMessage("Name is mandatory.")
                .MinimumLength(3).WithMessage("Name should be minimum 3 character.");
        }
    }
}
