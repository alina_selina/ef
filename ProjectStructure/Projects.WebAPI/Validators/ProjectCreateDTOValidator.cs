﻿using FluentValidation;
using Projects.Common.DTO;

namespace Projects.WebAPI.Validators
{
    public class ProjectCreateDTOValidator : AbstractValidator<ProjectCreateDTO>
    {
        public ProjectCreateDTOValidator()
        {
            RuleFor(t => t.Name)
                .NotEmpty().WithMessage("Name is mandatory.")
                .MinimumLength(5).WithMessage("Name should be minimum 5 character.")
                .MaximumLength(100).WithMessage("Name should be maximum 100 character.");

            RuleFor(t => t.AuthorId)
                .NotEmpty().WithMessage("Author is mandatory.");

            RuleFor(t => t.TeamId)
                .NotEmpty().WithMessage("Team is mandatory.");

            RuleFor(t => t.Deadline)
                .NotEmpty().WithMessage("Deadline is mandatory.");
        }
    }
}
