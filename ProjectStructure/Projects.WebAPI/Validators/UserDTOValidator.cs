﻿using FluentValidation;
using Projects.Common.DTO;

namespace Projects.WebAPI.Validators
{
    public class UserDTOValidator : AbstractValidator<UserCreateDTO>
    {
        public UserDTOValidator()
        {
            RuleFor(u => u.FirstName)
                .NotEmpty().WithMessage("FirstName is mandatory.")
                .MinimumLength(3).WithMessage("FirstName should be minimum 3 character.");

            RuleFor(u => u.LastName)
                .NotEmpty().WithMessage("LastName is mandatory.")
                .MinimumLength(3).WithMessage("LastName should be minimum 3 character.");

            RuleFor(u => u.Email)
                .EmailAddress();

            RuleFor(u => u.BirthDay)
                .NotEmpty().WithMessage("BirthDay is mandatory.");
        }
    }
}
