﻿using FluentValidation;
using Projects.Common.DTO.Task;

namespace Projects.WebAPI.Validators
{
    public class TaskCreateDTOValidator : AbstractValidator<TaskCreateDTO>
    {
        public TaskCreateDTOValidator()
        {
            RuleFor(t => t.Name)
                .NotEmpty().WithMessage("Name is mandatory.")
                .MinimumLength(10).WithMessage("Name should be minimum 10 character.")
                .MaximumLength(100).WithMessage("Name should be maximum 100 character.");

            RuleFor(t => t.PerformerId)
                .NotEmpty().WithMessage("Performer is mandatory.");

            RuleFor(t => t.ProjectId)
                .NotEmpty().WithMessage("Project is mandatory.");
        }
    }
}
