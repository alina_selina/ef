﻿using AutoMapper;
using Projects.WebAPI.Controllers.Abstract;
using Projects.BLL.Interfaces;
using Projects.Common.DTO;

namespace Projects.WebAPI.Controllers
{
    public class ProjectsController : AbstractController<IProjectService, ProjectDTO, ProjectCreateDTO>
    {
        public ProjectsController(IProjectService service, IMapper mapper) : base(service, mapper)
        {
        }
    }
}
