﻿using Microsoft.AspNetCore.Mvc;
using Projects.BLL.Interfaces;
using Projects.Common.DTO.Query;
using Projects.Common.DTO.Task;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Projects.WebAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class QueriesController : ControllerBase
    {
        private readonly IQueryService _service;

        public QueriesController(IQueryService service)
        {
            _service = service;
        }

        [HttpGet("ProjectTaskCount/{id}")]
        public async Task<ActionResult<IEnumerable<ProjectTasksDTO>>> GetProjectTaskCount(int id)
        {
            return Ok(await _service.GetProjectTaskCountByUserIdAsync(id));
        }

        [HttpGet("UserTasks/{id}")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetUserTasks(int id)
        {
            return Ok(await _service.GetUserTasksByUserIdAsync(id));
        }

        [HttpGet("UserTasksCurrentYear/{id}")]
        public async Task<ActionResult<IEnumerable<UserTasksCurrentYearDTO>>> GetUserTasksCurrentYear(int id)
        {
            return Ok(await _service.GetUserTasksCurrentYearByUserIdAsync(id));
        }

        [HttpGet("TeamsByCondition")]
        public async Task<ActionResult<IEnumerable<TeamsDTO>>> GetTeams()
        {
            return Ok(await _service.GetTeamsAsync());
        }

        [HttpGet("UsersWithTasks")]
        public async Task<ActionResult<IEnumerable<UsersDTO>>> GetUsers()
        {
            return Ok(await _service.GetUsersAsync());
        }

        [HttpGet("UserStruct/{id}")]
        public async Task<ActionResult<UserStructDTO>> GetUserStruct(int id)
        {
            return Ok(await _service.GetUserStructAsync(id));
        }

        [HttpGet("ProjectStruct/{id}")]
        public async Task<ActionResult<ProjectStructDTO>> GetProjectStruct(int id)
        {
            return Ok(await _service.GetProjectStructAsync(id));
        }

        [HttpGet("UnfinishedTasks/{id}")]
        public async Task<ActionResult<IEnumerable<TaskDTO>>> GetUnfinishedTasks(int id)
        {
            return Ok(await _service.GetUnfinishedTasksByUserIdAsync(id));
        }
    }
}
