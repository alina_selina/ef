﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Projects.BLL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Projects.WebAPI.Controllers.Abstract
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public abstract class AbstractController<TService, TDto, TCreateDto> : ControllerBase
                                        where TService : IService<TDto, TCreateDto>
                                        where TDto : class where TCreateDto : class
    {
        protected readonly TService _service;
        protected readonly IMapper _mapper;

        protected AbstractController(TService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        // GET: api/EntityName
        [HttpGet]
        public virtual async Task<ActionResult<IEnumerable<TDto>>> Get()
        {
            return Ok(await _service.GetAllAsync());
        }

        // GET: api/EntityName/1
        [HttpGet("{id}")]
        public virtual async Task<ActionResult<TDto>> GetById(int id)
        {
            return Ok(await _service.GetByIdAsync(id));
        }

        // POST: api/EntityName
        [HttpPost]
        public virtual async Task<ActionResult<TDto>> Create([FromBody] TCreateDto dto)
        {
            return Ok(await _service.CreateAsync(dto));
        }

        // PUT: api/EntityName
        [HttpPut]
        public virtual async Task<ActionResult<TDto>> Update([FromBody] TDto dto)
        {
            return Ok(await _service.UpdateAsync(dto));
        }

        // DELETE: api/EntityName/1
        [HttpDelete("{id}")]
        public virtual async Task<ActionResult> Delete(int id)
        {
            await _service.DeleteAsync(id);
            return NoContent();
        }
    }
}
