﻿using System;
using Microsoft.EntityFrameworkCore;
using Projects.DAL;
using Projects.DAL.Data;
using Projects.DAL.Interfaces;

namespace Projects.BLL.Tests
{
    public class DatabaseFixture : IDisposable
    {
        public IUnitOfWork Uow { get; }
        private ProjectsContext _context;

        public DatabaseFixture()
        {
            var options = new DbContextOptionsBuilder<ProjectsContext>()
                             .UseInMemoryDatabase("ProjectsDbTest")
                             .Options;
            _context = new ProjectsContext(options);

            DataManager.Seed(_context);

            Uow = new UnitOfWork(_context);
        }

        public void Dispose()
        {
            DataManager.ClearContext(_context);
            Uow?.Dispose();
        }
    }
}
