﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projects.BLL.Exceptions;
using Projects.BLL.Interfaces;
using Projects.BLL.Services;
using Xunit;

namespace Projects.BLL.Tests
{
    [Collection("Non-Parallel Collection")]
    public class QueryServiceTests : IClassFixture<DatabaseFixture>
    {
        private readonly IQueryService _service;

        public QueryServiceTests(DatabaseFixture fixture)
        {
            _service = new QueryService(fixture.Uow, Mapper.GetMapper());
        }

        [Fact]
        public async void GetProjectTaskCountByUserId_WhenUserIdIs4_ThenProjectIdIs16TaskCountIs3()
        {
            var result = await _service.GetProjectTaskCountByUserIdAsync(4);

            Assert.Equal(16, result.FirstOrDefault()?.Project.Id);
            Assert.Equal(3, result.FirstOrDefault()?.TaskCount);
        }

        [Fact]
        public async void GetUserTasksByUserId_WhenUserIdIs4_ThenTaskCountIs1WithIdIs1()
        {
            var result = await _service.GetUserTasksByUserIdAsync(4);

            Assert.Single(result);
            Assert.Equal(1, result.FirstOrDefault()?.Id);
        }

        [Fact]
        public async void GetUserTasksCurrentYearByUserId_WhenUserIdIs4_ThenTaskCountIs1WithIdIs1()
        {
            var result = await _service.GetUserTasksCurrentYearByUserIdAsync(4);

            Assert.Single(result);
            Assert.Equal(1, result.FirstOrDefault()?.Id);
        }

        [Fact]
        public async void GetUserTasksCurrentYearByUserId_WhenUserIdIs19_ThenTaskCountIs0()
        {
            var result = await _service.GetUserTasksCurrentYearByUserIdAsync(19);

            Assert.Empty(result);
        }

        [Fact]
        public async void GetTeams_ThenTeamsCountIs12WithCorrectIds()
        {
            var expectedTeamIds = new List<int> { 1, 2, 3, 5, 8, 9, 10, 12, 16, 18, 19, 20 };
            var sortedUserIds = new List<int> { 6, 2, 12 };

            var result = await _service.GetTeamsAsync();

            Assert.Equal(expectedTeamIds, result.OrderBy(i => i.Id).Select(t => t.Id).ToList());
            Assert.Equal(sortedUserIds, result.Where(t => t.Id == 12).First().Users.Select(u => u.Id).ToList());
            Assert.Equal(12, result.Count());
        }

        [Fact]
        public async void GetUsers_ThenUsersSortedByFirstNameWithTasksSortedByName()
        {
            var sortedUserNames = new List<string> { "Boyd", "Eloise", "Erma", "Genevieve" };
            var firstUserTaskNames = new List<string>
            {
                "Odit corporis corporis totam libero sunt sunt nemo neque deleniti.",
                "Provident quas rem et cupiditate laudantium temporibus voluptas.",
                "Odio nulla beatae at est est doloremque reprehenderit.",
                "Officiis assumenda illum ipsum quasi veritatis alias.",
                "Libero voluptatibus ratione minus non.",
                "Alias labore cumque est.",
                "Numquam ut odio."
            };

            var result = await _service.GetUsersAsync();

            Assert.Equal(sortedUserNames, result.Take(4).Select(t => t.User.FirstName).ToList());
            Assert.Equal(firstUserTaskNames, result.First().Tasks.Select(t => t.Name).ToList());
        }

        [Fact]
        public async void GetUserStruct_WhenUserIdIs4_ThenUserStructWithCorrectResults()
        {
            var result = await _service.GetUserStructAsync(4);

            Assert.Equal(4, result.User.Id);
            Assert.Equal(16, result.LastProject.Id);
            Assert.Equal(3, result.LastProjectTaskCount);
            Assert.Equal(0, result.UnfinishedTaskCount);
            Assert.Equal(1, result.LongestTask.Id);
        }

        [Fact]
        public async void GetProjectStruct_WhenProjectIdIs5_ThenProjectStructWithCorrectResults()
        {
            var result = await _service.GetProjectStructAsync(5);

            Assert.Equal(5, result.Project.Id);
            Assert.Equal(19, result.LongestTask.Id);
            Assert.Equal(7, result.ShortestTask.Id);
            Assert.Equal(1, result.UserCount);
        }

        [Fact]
        public async void GetUnfinishedTasksByUserId_WhenUserIdIs20_ThenOneTaskWithId6()
        {
            var result = await _service.GetUnfinishedTasksByUserIdAsync(20);

            Assert.Single(result);
            Assert.Equal(6, result.First().Id);
        }

        [Fact]
        public async void GetUnfinishedTasksByUserId_WhenUserIdIs19_ThenEmptyList()
        {
            var result = await _service.GetUnfinishedTasksByUserIdAsync(19);

            Assert.Empty(result);
        }

        [Fact]
        public void GetUnfinishedTasksByUserId_WhenUnexistingUser_ThenHttpException()
        {
            Assert.ThrowsAsync<InvalidArgumentException>(async () => await _service.GetUnfinishedTasksByUserIdAsync(22));
        }
    }
}
