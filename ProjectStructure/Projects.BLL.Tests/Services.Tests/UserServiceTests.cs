using Microsoft.EntityFrameworkCore;
using Projects.BLL.Exceptions;
using Projects.BLL.Interfaces;
using Projects.BLL.Services;
using Projects.DAL;
using Projects.DAL.Data;
using Projects.DAL.Interfaces;
using System;
using System.Linq;
using Xunit;

namespace Projects.BLL.Tests
{
    public class UserServiceTests : IDisposable
    {
        private ProjectsContext _context;
        private IUnitOfWork _uow;
        private IUserService _service;

        public UserServiceTests()
        {
            var options = new DbContextOptionsBuilder<ProjectsContext>()
                             .UseInMemoryDatabase("TestProjectsDb")
                             .Options;
            _context = new ProjectsContext(options);
            _context.Database.EnsureCreated();

            _uow = new UnitOfWork(_context);
            _service = new UserService(_uow, Mapper.GetMapper());
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _uow.Dispose();
        }

        [Theory]
        [InlineData("name", "surname", "2000-01-01T05:10:18.898869+00:00", "test@gmail.com", null)]
        [InlineData("name", "surname", "2000-01-01T05:10:18.898869+00:00", "test@gmail.com", 1)]
        public async void Create_WhenNewUserWithNullTeamOrExistingTeam_ThenCreatedUser(string name, string surname, string date, string email, int? team)
        {
            var user = new Common.DTO.UserCreateDTO()
            {
                FirstName = name,
                LastName = surname,
                BirthDay = DateTime.Parse(date),
                Email = email,
                TeamId = team
            };

            var result = await _service.CreateAsync(user);
            var users = await _service.GetAllAsync();

            Assert.Equal(user.Email, result.Email);
            Assert.Equal(21, users.Count());
        }

        [Fact]
        public void Create_WhenNewUserWithUnexistingTeam_ThenNotFoundError()
        {
            var user = new Common.DTO.UserCreateDTO()
            {
                FirstName = "name",
                LastName = "surname",
                BirthDay = new DateTime(2000, 2, 1),
                Email = "test@gmail.com",
                TeamId = 21
            };

            Assert.ThrowsAsync<InvalidArgumentException>(async () => await _service.CreateAsync(user));
        }

        [Fact]
        public async void Update_WhenUserWithExistingTeam_ThenUpdatedUser()
        {
            var userToUpdate = await _service.GetByIdAsync(1);
            userToUpdate.TeamId = 15;

            var updatedUser = await _service.UpdateAsync(userToUpdate);

            Assert.Equal(userToUpdate.FirstName, updatedUser.FirstName);
            Assert.Equal(userToUpdate.LastName, updatedUser.LastName);
            Assert.Equal(userToUpdate.Email, updatedUser.Email);
            Assert.Equal(userToUpdate.TeamId, updatedUser.TeamId);
        }

        [Fact]
        public async void Update_WhenUserWithUnexistingTeam_ThenNotFoundError()
        {
            var userToUpdate = await _service.GetByIdAsync(1);
            userToUpdate.TeamId = 21;

            Assert.ThrowsAsync<InvalidArgumentException>(async () => await _service.UpdateAsync(userToUpdate));
        }
    }
}
