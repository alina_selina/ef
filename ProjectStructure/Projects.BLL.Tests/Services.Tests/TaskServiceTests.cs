﻿using Microsoft.EntityFrameworkCore;
using Projects.BLL.Exceptions;
using Projects.BLL.Interfaces;
using Projects.BLL.Services;
using Projects.Common.DTO.Task;
using Projects.DAL;
using Projects.DAL.Data;
using Projects.DAL.Interfaces;
using System;
using Xunit;

namespace Projects.BLL.Tests
{
    [Collection("Non-Parallel Collection")]
    public class TaskServiceTests : IDisposable
    {
        private ProjectsContext _context;
        private IUnitOfWork _uow;
        private ITaskService _service;

        public TaskServiceTests()
        {
            var options = new DbContextOptionsBuilder<ProjectsContext>()
                             .UseInMemoryDatabase("TestProjectsDb")
                             .Options;
            _context = new ProjectsContext(options);
            _context.Database.EnsureCreated();

            _uow = new UnitOfWork(_context);
            _service = new TaskService(_uow, Mapper.GetMapper());
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _uow.Dispose();
        }

        [Fact]
        public void Create_WhenTaskWithUnexistingProject_ThenNotFoundError()
        {
            var task = new TaskCreateDTO()
            {
                Name = "task",
                State = Common.Enums.TaskState.ToDo,
                Description = "text",
                PerformerId = 1,
                ProjectId = 21
            };

            Assert.ThrowsAsync<InvalidArgumentException>(async () => await _service.CreateAsync(task));
        }

        [Fact]
        public async void Update_WhenTaskWithUnexistingState_ThenNotFoundError()
        {
            var task = await _service.GetByIdAsync(1);
            task.State = (Common.Enums.TaskState)6;

            Assert.ThrowsAsync<InvalidArgumentException>(async () => await _service.UpdateAsync(task));
        }

        [Fact]
        public async void Update_WhenTaskWithDoneState_ThenUpdatedTask()
        {
            var task = await _service.GetByIdAsync(1);
            task.State = Common.Enums.TaskState.Done;

            var result = await _service.UpdateAsync(task);

            Assert.Equal(Common.Enums.TaskState.Done, result.State);
        }
    }
}
