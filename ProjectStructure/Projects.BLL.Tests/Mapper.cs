﻿using AutoMapper;
using Projects.BLL.MappingProfiles;

namespace Projects.BLL.Tests
{
    public static class Mapper
    {
        public readonly static IMapper _mapper;

        static Mapper()
        {
            _mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<ProjectProfile>();
            }).CreateMapper();
        }

        public static IMapper GetMapper() => _mapper;
    }
}
