# Entity Framework #

Используя приложение из предыдущего задания, необходимо добавить использование базы данных.

Требования:

* Вместо хранения объектов в памяти - нужно подключить базу данных
* Добавить Initial миграцию, которая будет создавать базу с исходной структурой
* Добавить две миграции с любыми изменениями, будь то изменения связей или переименование колонок
* Добавить seeding
* Добавить базовую валидацию моделей используя DataAnnotations или FluentAPI

# UPD #

Aсинхронная версия - [develop](https://bitbucket.org/alina_selina/ef/src/develop/)

Тесты - [feature/add-integrationTests](https://bitbucket.org/alina_selina/ef/src/522cd550384c3b2a83826081304beafd6db7bb26/?at=feature%2Fadd-integrationTests) (асинхронные - [develop](https://bitbucket.org/alina_selina/ef/src/develop/))